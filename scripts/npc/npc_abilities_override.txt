// Dota Abilities Override File
"DOTAAbilities"
{
  "Version"    "1"
  
  //=================================================================================================================
  // Alchemist: Juxtapose
  //=================================================================================================================
  "phantom_lancer_juxtapose" 
  {
    "AbilityType"    "DOTA_ABILITY_TYPE_BASIC"
    "MaxLevel"       "4"
    
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"           "FIELD_INTEGER"
        "max_illusions"      "5 7 9 9"
      }
      "02"
      {
        "var_type"           "FIELD_INTEGER"
        "proc_chance_pct"    "40 45 50 50"
      }
    }
  }
  
  //=================================================================================================================
  // Alchemist: Blink Strike
  //=================================================================================================================
  "riki_blink_strike"
  {
    "AbilityType"    "DOTA_ABILITY_TYPE_BASIC"
    "MaxLevel"       "4"
    
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"         "FIELD_INTEGER"
        "tooltip_range"    "800"
      }
      "02"
      {
        "var_type"         "FIELD_INTEGER"
        "bonus_damage"     "40 70 100 120"
      }
      "03"
      {
        "var_type"         "FIELD_INTEGER"
        "max_charges"      "4 5 6 7"
      }
      "04"
      {
        "var_type"               "FIELD_FLOAT"
        "charge_restore_time"    "35.0"
      }
    }
  }
  
  //=================================================================================================================
  // Omniknight: Shadow Wave
  //=================================================================================================================
  "dazzle_shadow_wave"
  {
    "AbilityCooldown"        "1.5 1.5 1.5 1.5"
    "AbilityManaCost"        "70 70 70 70"
  }
  
  //=================================================================================================================
  // Windrunner: Impetus
  //=================================================================================================================
  "enchantress_impetus"
  {
    "AbilityManaCost"        "20 20 20"
  }
  
  //=================================================================================================================
  // Zeus: Maledict
  //=================================================================================================================
  "witch_doctor_maledict"
  {
    "AbilityCastRange"        "650"
  }
  
  //=================================================================================================================
  // Rubick: Boulder Smash (Earth Spirit)
  //=================================================================================================================
  "earth_spirit_boulder_smash"
  {
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
  }
  
  //=================================================================================================================
  // Rubick: Breath Fire (Fire Ward)
  //=================================================================================================================
  "dragon_knight_breathe_fire"
  {
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
    "AbilityDamage"          "400 500 600 700"
  }
  
  //=================================================================================================================
  // Rubick: LSA
  //=================================================================================================================
  "lina_light_strike_array"
  {
    "AbilityCastRange"        "0"
    "AbilityManaCost"        "0 0 0 0"
  }
  
  //=================================================================================================================
  // Rubick: Firefly
  //=================================================================================================================
  "batrider_firefly"
  {
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
    
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"             "FIELD_INTEGER"
        "damage_per_second"    "10 20 30 40"
      }
      "02"
      {
        "var_type"            "FIELD_INTEGER"
        "radius"              "150 150 150 150"
      }
      "03"
      {
        "var_type"         "FIELD_FLOAT"
        "duration"         "100 100 100 100"
      }
      "04"
      {
        "var_type"         "FIELD_FLOAT"
        "tick_interval"    "0.5 0.5 0.5 0.5"
      }
      "05"
      {
        "var_type"         "FIELD_INTEGER"
        "tree_radius"      "100 100 100 100"
      }
    }
  }
  
  //=================================================================================================================
  // Rubick: BurrowStrike
  //=================================================================================================================
  "sandking_burrowstrike"
  {
    "AbilityCastRange"       "1000"
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
  }
  
  //=================================================================================================================
  // Rubick: Illusory Orb
  //=================================================================================================================
  "puck_illusory_orb"
  {
    "AbilityUnitDamageType"    "DAMAGE_TYPE_COMPOSITE"
    "AbilityBehavior"          "DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
    
    "AbilityCastRange"        "0"
    "AbilityCastPoint"        "-0.1"
    "AbilityCastAnimation"    "ACT_DOTA_DISABLED"
    
    "AbilityCooldown"         "0 0 0 0"
    "AbilityManaCost"         "0 0 0 0"
  }
  
  //=================================================================================================================
  // Pudge: Meat Hook
  //=================================================================================================================
  "pudge_meat_hook"
  {
    "AbilityBehavior"        "DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
    
    "AbilityCastRange"        "1450"
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
    
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"        "FIELD_FLOAT"
        "hook_speed"      "2000.0"
      }
    }
  }
  
  //=================================================================================================================
  // Pudge: Dismember
  //=================================================================================================================
  "pudge_dismember"
  {
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
  }
  
  //=================================================================================================================
  // Pudge: Toss (Earth Spirit)
  //=================================================================================================================
  "tiny_toss"
  {
    "AbilityCastRange"        "5000"
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
  }
  
  //=================================================================================================================
  // Pudge: Crush
  //=================================================================================================================
  "slardar_slithereen_crush"
  {
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
  }
  
  //=================================================================================================================
  // Kunkka: Torrent
  //=================================================================================================================
  "kunkka_torrent"
  {
    "AbilityCastRange"        "0"
    "AbilityCooldown"        "0 0 0 0"
    "AbilityManaCost"        "0 0 0 0"
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"          "FIELD_INTEGER"
        "radius"          "225"
      }
      "02"
      {
        "var_type"          "FIELD_INTEGER"
        "movespeed_bonus"      "-20"
      }
      "03"
      {
        "var_type"          "FIELD_FLOAT"
        "slow_duration"        "1.0 2.0 3.0 4.0"
      }
      "04"
      {
        "var_type"          "FIELD_FLOAT"
        "stun_duration"        "0.7"
      }
      "05"
      {
        "var_type"          "FIELD_FLOAT"
        "delay"            "1.3 1.3 1.3 1.3"
      }
    }  
  }
  
  //=================================================================================================================
  // Kunkka: Pounce (Mini-Slarks)
  //=================================================================================================================
  "slark_pounce"
  {
    
    "AbilityCooldown"        "0"
    "AbilityManaCost"        "0"
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"        "FIELD_INTEGER"
        "pounce_distance"    "700"
      }
      "02"
      {
        "var_type"        "FIELD_FLOAT"
        "pounce_speed"      "933.33"
      }
      "03"
      {
        "var_type"        "FIELD_FLOAT"
        "pounce_acceleration"  "7000.0"
      }
      "04"
      {
        "var_type"        "FIELD_INTEGER"
        "pounce_radius"      "95"
      }
      "05"
      {
        "var_type"        "FIELD_INTEGER"
        "pounce_damage"      "240"
      }
      "06"
      {
        "var_type"        "FIELD_FLOAT"
        "leash_duration"    "1.5 2 2.5 3"
      }
      "07"
      {
        "var_type"        "FIELD_INTEGER"
        "leash_radius"      "325"
      }
    }
  }
  
  //=================================================================================================================
  // Kunkka: Tidebringer
  //=================================================================================================================
  "kunkka_tidebringer"
  {
    
    "AbilityCooldown"        "0"
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"          "FIELD_INTEGER"
        "radius"          "500 600 700 800"
      }
      "02"
      {
        "var_type"          "FIELD_INTEGER"
        "damage_bonus"        "60 120 180 300"
      }
    }
  }
  
  //=================================================================================================================
  // Kunkka: X Marks the Spot
  //=================================================================================================================
  "kunkka_x_marks_the_spot"
  {
    "AbilityCastRange"      "0"
    "AbilityCastPoint"      "0.001"
    "AbilityCooldown"       "0"
    "AbilityManaCost"       "0"
  }
  
  // -----------------------------------------------------
  // -----------------------------------------------------
  // -----------------------------------------------------
  //             ITEMS
  // -----------------------------------------------------
  // -----------------------------------------------------
  // -----------------------------------------------------
  
  "item_courier"            "REMOVE"
  "item_flying_courier"     "REMOVE"
  "item_smoke_of_deceit"    "REMOVE"
  "item_black_king_bar"     "REMOVE"
  "item_magic_stick"        "REMOVE"
  "item_magic_wand"         "REMOVE"
  "item_sheepstick"         "REMOVE"
  "item_basher"             "REMOVE"
  "item_abyssal_blade"      "REMOVE"
  "item_tpscroll"           "REMOVE"
  "item_gem"                "REMOVE"
  "item_orchid"             "REMOVE"
  "item_hand_of_midas"      "REMOVE"
  "item_heavens_halberd"    "REMOVE"
  
}
