// Dota Heroes File
// NOTE: This isn't for creating new heroes, this is for taking a currently existing hero as a template and overriding
// the specified key-value combinations.  Use override_hero <hero_to_override> for this.
"DOTAHeroes"
{
  
  //=================================================================================================================
  // Alchemist
  //=================================================================================================================
  "npc_dota_hero_tank"
  {
    "override_hero"        "npc_dota_hero_alchemist"
    
    "AbilityLayout"     "5"
    "Ability1"          "riki_blink_strike"
    "Ability2"          "phantom_lancer_juxtapose"
    "Ability3"          "spectre_dispersion"
    "Ability4"          "troll_warlord_fervor"
    "Ability5"          "alchemist_chemical_rage"
    
    "StatusHealth"         "2200"
    "StatusHealthRegen"    "25"
    "StatusMana"           "150"
    "StatusManaRegen"      "1"
  }
  
  //=================================================================================================================
  // Omniknight
  //=================================================================================================================
  "npc_dota_hero_priest"
  {
    "override_hero"        "npc_dota_hero_omniknight"
    
    "AbilityLayout"     "5"
    "Ability1"          "omniknight_purification"
    "Ability2"          "dazzle_shadow_wave"
    "Ability3"          "rubick_null_field"
    "Ability4"          "skeleton_king_vampiric_aura"
    "Ability5"          "pugna_life_drain"
    
    "StatusHealth"        "1500"
    "StatusHealthRegen"      "7"
    "StatusMana"        "1500"
    "StatusManaRegen"      "10"
  }
  
  //=================================================================================================================
  // Undying
  //=================================================================================================================
  "npc_dota_hero_buffer"
  {
    "override_hero"        "npc_dota_hero_undying"
    
    "AbilityLayout"     "5"
    "Ability1"          "legion_commander_press_the_attack"
    "Ability2"          "ogre_magi_bloodlust"
    "Ability3"          "bane_brain_sap"
    "Ability4"          "spirit_breaker_empowering_haste"
    "Ability5"          "undying_flesh_golem"
    
    "StatusHealth"         "2500"
    "StatusHealthRegen"    "5"
    "StatusMana"           "1000"
    "StatusManaRegen"      "10"
  }
  
  //=================================================================================================================
  // Windrunner
  //=================================================================================================================
  "npc_dota_hero_physical_dps"
  {
    "override_hero"        "npc_dota_hero_windrunner"
    
    "AbilityLayout"     "5"
    "Ability1"          "templar_assassin_refraction"
    "Ability2"          "ursa_overpower"
    "Ability3"          "skeleton_king_mortal_strike"
    "Ability4"          "faceless_void_backtrack"
    "Ability5"          "enchantress_impetus"

    "StatusHealth"        "1500"
    "StatusHealthRegen"      "1"
    "StatusMana"        "1300"
    "StatusManaRegen"      "6"
    
  }
  
  //=================================================================================================================
  // Zeus
  //=================================================================================================================
  "npc_dota_hero_magic_dps"
  {
    "override_hero"        "npc_dota_hero_zuus"
    
    "AbilityLayout"    "5"
    "Ability1"         "zuus_arc_lightning"
    "Ability2"         "witch_doctor_maledict"
    "Ability3"         "visage_soul_assumption"
    "Ability4"         "puck_phase_shift"
    "Ability5"         "skywrath_mage_mystic_flare"
    
    "StatusHealth"         "1500"
    "StatusHealthRegen"    "1"
    "StatusMana"           "1500"
    "StatusManaRegen"      "35"
  }
  
  //=================================================================================================================
  // Rubick
  //=================================================================================================================
  "npc_dota_hero_magus_the"
  {
    "override_hero"     "npc_dota_hero_rubick"
    "AbilityLayout"     "6"
    "Ability1"          "rubick_boss_earth_toten"
    "Ability2"          "rubick_boss_sphere"
    "Ability3"          "lina_light_strike_array"
    "Ability4"          "batrider_firefly"
    "Ability5"          "sandking_burrowstrike"
    "Ability6"          "rubick_boss_fire_ward"
    "Ability7"          "dota_creature_no_healbar"
    
    "StatusHealth"         "10000"
    "StatusHealthRegen"    "0"
    "StatusMana"           "10000"
    "StatusManaRegen"      "150"
    
    "ArmorPhysical"        "25"
    "MagicalResistance"    "50"
    
    "AttackCapabilities"    "DOTA_UNIT_CAP_NO_ATTACK"
    "MovementTurnRate"      "1.4"
    
    "ModelScale"    "1.3"
    
    "AttributePrimary"             "DOTA_ATTRIBUTE_INTELLECT"
    "AttributeBaseStrength"        "0"
    "AttributeStrengthGain"        "512"
    "AttributeBaseIntelligence"    "0"
    "AttributeIntelligenceGain"    "0"
    "AttributeBaseAgility"         "0"
    "AttributeAgilityGain"         "0"
  }
  
  //=================================================================================================================
  // Rubick: Earth Spirit
  //=================================================================================================================
  "npc_dota_hero_earth_dude"
  {
    "override_hero"         "npc_dota_hero_earth_spirit"
    "Ability2"              "dota_creature_invulnerable"
    "Ability3"              "tiny_toss"
    "Ability4"              "dota_creature_collision"
    "Ability5"              "kunkka_torrent"
    "Ability6"              "kunkka_x_marks_the_spot"
    "AttackCapabilities"    "DOTA_UNIT_CAP_NO_ATTACK"
    
    
    "StatusHealth"         "10000"
    "StatusHealthRegen"    "0"
    "StatusMana"           "10000"
    "StatusManaRegen"      "150"
  }
  
  //=================================================================================================================
  // Pudge
  //=================================================================================================================
  "npc_dota_hero_fatty"
  {
    "override_hero"    "npc_dota_hero_pudge"
    "AbilityLayout"    "6"
    "Ability3"         "bloodseeker_thirst"
    "Ability5"         "pudge_boss_spawn_minipudges"
    "Ability6"         "slardar_slithereen_crush"
    "Ability7"         "dota_creature_no_healbar"
    
    "StatusHealth"         "10000"
    "StatusHealthRegen"    "75"
    "StatusMana"           "10000"
    "StatusManaRegen"      "150"
    
    "ArmorPhysical"        "25"
    "MagicalResistance"    "50"
    
    "AttackDamageMin"      "300"
    "AttackDamageMax"      "400"
    
    "MovementCapabilities"    "DOTA_UNIT_CAP_MOVE_FLY"
    "MovementTurnRate"        "1"
    "MovementSpeed"           "300"
    
    "ModelScale"         "1.7"
    
    "AttributePrimary"             "DOTA_ATTRIBUTE_INTELLECT"
    "AttributeBaseStrength"        "0"
    "AttributeStrengthGain"        "512"
    "AttributeBaseIntelligence"    "0"
    "AttributeIntelligenceGain"    "50"
    "AttributeBaseAgility"         "0"
    "AttributeAgilityGain"         "0"
  }
  
  //=================================================================================================================
  // Kunkka
  //=================================================================================================================
  "npc_dota_hero_admiral"
  {
    "override_hero"    "npc_dota_hero_kunkka"
    "Ability1"         "dota_creature_no_healbar"
    
    "StatusHealth"         "10000"
    "StatusHealthRegen"    "0"
    "StatusMana"           "10000"
    "StatusManaRegen"      "150"
    
    "ArmorPhysical"        "25"
    "MagicalResistance"    "50"
    
    "AttackDamageMin"      "150"
    "AttackDamageMax"      "250"
    
    "MovementCapabilities"    "DOTA_UNIT_CAP_MOVE_NONE"
    "MovementTurnRate"        "1.2"
    
    "ModelScale"         "1"
    
    "AttributePrimary"             "DOTA_ATTRIBUTE_INTELLECT"
    "AttributeBaseStrength"        "0"
    "AttributeStrengthGain"        "527"
    "AttributeBaseIntelligence"    "0"
    "AttributeIntelligenceGain"    "50"
    "AttributeBaseAgility"         "0"
    "AttributeAgilityGain"         "0"
  }
  
  
}