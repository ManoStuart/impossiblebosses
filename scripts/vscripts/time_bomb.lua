function Precache( context )
end
 
function Activate()
  Timers:CreateTimer(3, Slap)
  Timers:CreateTimer(4.5, Destroy)
end

function Destroy()
    if not thisEntity:IsNull() then
        thisEntity:Destroy()
    end
end

function Slap()
  if not thisEntity:IsAlive() then
    thisEntity:Destroy()
    return
  end

  local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil, -1, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
  if #allEnemies > 0 then
    local index = RandomInt( 1, #allEnemies )
    target = allEnemies[index]
  end

  local rand = RandomInt(0,3)
  local slaped = thisEntity
  if rand >= 1 then
    slaped = Entities:FindByClassname( nil, "npc_dota_hero_rubick")
  end

  if target then
    local position = (slaped:GetOrigin() - target:GetOrigin())
    local len = position:Length() 
    position = slaped:GetOrigin() + (position * 100) / len
    GameRules.earthSpirit:SetAbsOrigin(position)

    local smash = GameRules.earthSpirit:FindAbilityByName("earth_spirit_boulder_smash")
    local order = 
    {
      queue = true,
      OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
      UnitIndex = GameRules.earthSpirit:entindex(),
      Position = slaped:GetOrigin(),
      TargetIndex = slaped:entindex(),
      AbilityIndex = smash:entindex()
    }
    ExecuteOrderFromTable( order )
  end

  Timers:CreateTimer(0.5, BlinkBack)
end

function BlinkBack ()
  GameRules.earthSpirit:SetAbsOrigin(Vector(0,0 -10000))
end