local hook
 
function Precache( context )
  PrecacheResource( "particle_folder", "particles/units/heroes/hero_alchemist", context )
end
-- Create the game mode when we activate
function Activate()
  Timers:CreateTimer(2.5, Explode)
  Timers:CreateTimer(0.1, Cyclone)
  Timers:CreateTimer(0.4, ModelScale)
end

function ModelScale()
  thisEntity:SetModelScale(0.6)
end

function Cyclone()
  local order =
  {
    OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
    UnitIndex = thisEntity:entindex(),
    TargetIndex = thisEntity:entindex(),
    AbilityIndex = thisEntity:GetItemInSlot(0):entindex()
  }
  ExecuteOrderFromTable( order )
end

function Explode()
  local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil, 250, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
  for _,enemy in pairs(allEnemies) do
    local damageTable =
    {
      victim = enemy,
      attacker = thisEntity,
      damage = 300,
      damage_type = DAMAGE_TYPE_PURE,
    }
    ApplyDamage(damageTable)
  end

  local order =
  {
    OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
    UnitIndex = thisEntity:entindex(),
    TargetIndex = thisEntity:entindex(),
    Position = thisEntity:GetOrigin(),
    AbilityIndex = thisEntity:FindAbilityByName("creature_rot_cloud"):entindex()
  }
  ExecuteOrderFromTable( order )

  ParticleManager:CreateParticle("particles/units/heroes/hero_alchemist/alchemist_unstable_concoction_explosion.vpcf",
                    PATTACH_ABSORIGIN, thisEntity)
  Timers:CreateTimer(0.5, Teleport)
  Timers:CreateTimer(60, Death)
end

function Teleport()
  thisEntity:SetAbsOrigin(Vector(0,0 -10000))
end

function Death()
  thisEntity:ForceKill(true)
end