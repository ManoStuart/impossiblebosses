function Precache( context )
end

-- Create the game mode when we activate
function Activate()
  thisEntity:AddNewModifier( thisEntity, nil, "modifier_invulnerable", {} )
  thisEntity:FindAbilityByName("naga_siren_song_of_the_siren"):SetLevel(3)
  
  Timers:CreateTimer(0.3, Sing)
end

function Sing ()
  if GameRules.BossScript:GetDeath() or not thisEntity then return end
  
  local target
  local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil, 500, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
  if #allEnemies > 0 then
    local index = RandomInt(1,#allEnemies)
    target = allEnemies[index]
  end
  
  if target then
    local order =
    {
      OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
      UnitIndex = thisEntity:entindex(),
      AbilityIndex = thisEntity:FindAbilityByName("naga_siren_song_of_the_siren"):entindex()
    }
    ExecuteOrderFromTable( order )
    
    Timers:CreateTimer(3.5, Death)
    return
  else
    return 0.5
  end
end

function Death()
  GameRules.BossScript.NagaDeath(thisEntity:entindex())
  thisEntity:Destroy()
end