local boss
local bossAi = {}
local order
local phase
local death = true

local curTombs = {}
local tombsPoints
local tombMax
local curTombnum

if PudgeBoss == nil then
  PudgeBoss = class({})

  if not GameRules.BossScripts then GameRules.BossScripts = {} end
  GameRules.BossScripts[2] = PudgeBoss()

  death = true
  Stats:setNumber("pudge", 0)
end

function PudgeBoss:GetBossName()
  return "PudgeBoss"
end

function PudgeBoss:Death ()
  death = true
  Timers:CreateTimer(10, GameRules.BossScript.DeathDestroy)
end

function PudgeBoss:AddStats ()
  Stats:inc("pudge", 1)
end

function PudgeBoss:DeathDestroy()
  local tombs = Entities:FindAllByModel("models/heroes/undying/undying_tower.vmdl")
  for _,k in pairs(tombs) do if k then k:ForceKill(true) end end

  local zombies = Entities:FindAllByModel("models/heroes/undying/undying_minion_torso.vmdl")
  for _,k in pairs(zombies) do if k then k:ForceKill(true) end end

  --local minis = Entities:FindAllByModel("models/heroes/pudge/pudge.vmdl")
  --for _,k in pairs(minis) do if k then k:ForceKill(true) end end

  boss:Destroy()
end

function PudgeBoss:GetDeath()
  return death
end

function PudgeBoss:GetBoss ()
  return boss
end

function PudgeBoss:GetBossAi ()
  return bossAi
end

function PudgeBoss:GetQuestInitiator()
  local quest =
  {
    name  = "#DOTA_Quest_Pudge_Init_Name",
    title = "#DOTA_Quest_Pudge_Init_Title",
    type = 0
  }
  return quest
end

function PudgeBoss:GetQuest()
  local quest =
  {
    name  = "#DOTA_Quest_Pudge_Combat_Name",
    title = "#DOTA_Quest_Pudge_Combat_Title",
    type = 2
  }
  return quest
end

function PudgeBoss:GetQuestFinale()
  local quest =
  {
    name  = "#DOTA_Quest_Clear_Name",
    title = "#DOTA_Quest_Pudge_Clear",
    type = 3
  }
  return quest
end

function PudgeBoss:Spawn()
  spawnPoint = Entities:FindByName(nil, "PudgeBossSpawn")
  boss = CreateUnitByName( "npc_dota_hero_pudge", spawnPoint:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
  
  bossAi.abilities = {}
  bossAi.abilities[1] = boss:FindAbilityByName("pudge_meat_hook")
  bossAi.abilities[2] = boss:FindAbilityByName("pudge_rot")
  bossAi.abilities[3] = boss:FindAbilityByName("pudge_dismember")
  bossAi.abilities[5] = boss:FindAbilityByName("slardar_slithereen_crush")
  bossAi.abilities[7] = boss:FindAbilityByName("dota_creature_no_healbar")
  for _,v in pairs(bossAi.abilities) do
    v:SetLevel(4)
  end
  bossAi.abilities[6] = boss:FindAbilityByName("bloodseeker_thirst")
  bossAi.abilities[6]:SetLevel(1)
  bossAi.abilities[4] = boss:FindAbilityByName("pudge_boss_spawn_minipudges")
  bossAi.abilities[4]:SetLevel(1)

  bossAi.patternTick = 0.1
  bossAi.curPattern = nil
  bossAi.curCastTime = 0
    
  bossAi.patterns = {}
  bossAi.patterns[1] = {on=true, cd=2, curCd=2, 
                            cast=GameRules.BossScript.Hook, message="Get over here!"}
  bossAi.patterns[2] = {on=true, cd=15, curCd=15, 
                            cast=GameRules.BossScript.Dismember, message="Fresh meat!"}

  bossAi.patterns[3] = {on=false, cd=14, curCd=14, 
                            cast=GameRules.BossScript.MiniPudges}
    bossAi.patterns[4] = {on=false, cd=4, curCd=4, 
                            cast=GameRules.BossScript.DropMiniPudge}
  bossAi.patterns[5] = {on=true, cd=12, curCd=12, 
                            cast=GameRules.BossScript.TinyToss}
    bossAi.patterns[6] = {on=false, cd=60, curCd=60, 
                            cast=GameRules.BossScript.HealingTomb, message="Come my servants!"}
  bossAi.patterns[7] = {on=true, cd=10, curCd=10, 
                            cast=GameRules.BossScript.Crush}

  tombsPoints = Entities:FindAllByName("tomb_spawn")
  curTombs = {}
  curTombnum = 0
  tombMax = #tombsPoints

  death = false
    phase = 0

  Timers:CreateTimer(1, GameRules.BossScript.CheckThrist)
  Timers:CreateTimer(0.3, GameRules.BossScript.ToogleRot)
  Timers:CreateTimer(0.5, GameRules.BossScript.PatternManager)
end

function PudgeBoss:ToogleRot()
  order =
  {
    OrderType = DOTA_UNIT_ORDER_CAST_TOGGLE,
    UnitIndex = boss:entindex(),
    AbilityIndex = bossAi.abilities[2]:entindex()
  }
  ExecuteOrderFromTable( order )
end

function PudgeBoss:CheckThrist()
  if death then return end

  local enemies = GameRules.heroes:GetHeroesEnt()

  for _,v in pairs(enemies) do
    if v:GetHealthPercent() < 50 then
      boss:SetRenderColor(255, 155, 155)
      return 1
    end
  end

  boss:SetRenderColor(255, 255, 255)
  return 1
end

function PudgeBoss:PatternManager ()
  if death then return end

  FireGameEvent( "impossible_update_boss_health", { healthPercent = boss:GetHealthPercent(), curHealth = boss:GetHealth()} )
    
    if     phase == 0 and boss:GetHealthPercent() < 80 then GameRules.BossScripts[2].Phase1() 
    elseif phase == 1 and boss:GetHealthPercent() < 60 then GameRules.BossScripts[2].Phase2()
    elseif phase == 2 and boss:GetHealthPercent() < 40 then GameRules.BossScripts[2].Phase3()
    elseif phase == 3 and boss:GetHealthPercent() < 20 then GameRules.BossScripts[2].Phase4()
    end

  for _,k in pairs(bossAi.patterns) do
    k.curCd = k.curCd - bossAi.patternTick
  end

  if not bossAi.curPattern then
    local cast
    for _,k in pairs(bossAi.patterns) do
      if k.curCd <= 0 and k.on then
        cast = k
      end
    end

    if cast then
      if (cast.message) then
        FireGameEvent( "impossible_boss_skill", {message = cast.message} )
      end
      bossAi.curCastTime = 0
      Timers:CreateTimer(bossAi.patternTick, cast.cast)
      bossAi.curPattern = cast
      cast.curCd = cast.cd
    end
  end

  return bossAi.patternTick
end

function PudgeBoss:Phase1()
    print("Pudge Phase1")
  bossAi.abilities[4]:SetLevel(2)
    bossAi.patterns[4].cd = 4
    bossAi.patterns[4].on = true
    
    phase = 1
end

function PudgeBoss:Phase2()
    print("Pudge Phase2")
  bossAi.abilities[4]:SetLevel(3)
  bossAi.abilities[6]:SetLevel(2)
  bossAi.patterns[3].cd = 14
    bossAi.patterns[3].on = true
    bossAi.patterns[6].cd = 60
    bossAi.patterns[6].on = true
    
    phase = 2
end

function PudgeBoss:Phase3()
    print("Pudge Phase3")
  bossAi.abilities[4]:SetLevel(4)
  bossAi.abilities[6]:SetLevel(3)
  bossAi.patterns[3].cd = 7
    bossAi.patterns[6].cd = 45
  local item = CreateItem("item_ultimate_scepter", boss, boss)
  boss:AddItem(item)
    
    phase = 3
end

function PudgeBoss:Phase4()
    print("Pudge Phase4")
    
    phase = 4
end

function PudgeBoss:MiniPudges ()
  if death then return end
  if bossAi.curCastTime == 0 then
    if bossAi.abilities[4] and bossAi.abilities[4]:IsFullyCastable() then
      order =
      {
        OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
        UnitIndex = boss:entindex(),
        AbilityIndex = bossAi.abilities[4]:entindex()
      }
      ExecuteOrderFromTable( order )
    end
    bossAi.curCastTime = 0.2
    return 0.2
  elseif bossAi.curCastTime == 0.2 then
    bossAi.curCastTime = 5
    return 5
  end

  bossAi.curPattern = nil
  return
end

function PudgeBoss:Hook ()
  if bossAi.abilities[1] and bossAi.abilities[1]:IsFullyCastable() then
    local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, boss:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end

  if target then
    order =
    {
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = boss:entindex(),
      Position = target:GetOrigin(),
      TargetIndex = target:entindex(),
      AbilityIndex = bossAi.abilities[1]:entindex()
    }
    ExecuteOrderFromTable( order )
  end

  bossAi.curPattern = nil
  return
end

local tossTarget
function PudgeBoss:TinyToss()
  if bossAi.curCastTime == 0 then
    local target
    local maxDist = 0
    if bossAi.abilities[3] and bossAi.abilities[3]:IsFullyCastable() then
      local allEnemies = GameRules.heroes:GetHeroesEnt()
      if #allEnemies > 0 then
        for _,enemy in pairs(allEnemies) do
          local distance = ( boss:GetOrigin() - enemy:GetOrigin() ):Length()
          if distance > maxDist then
            target = enemy
            maxDist = distance
          end
        end
      end
    end

    if target then
      tossTarget = CreateUnitByName( "npc_dota_dummy_target", target:GetOrigin(), false, nil, nil, DOTA_TEAM_GOODGUYS )
    else
      bossAi.curPattern = nil
      return
    end
    
    bossAi.curCastTime = 0.5
    return 0.5
  elseif bossAi.curCastTime == 0.5 then
    GameRules.earthSpirit:SetAbsOrigin(boss:GetOrigin())
    bossAi.curCastTime = 0.6
    return 0.1
  elseif bossAi.curCastTime == 0.6 then
    if tossTarget:IsAlive() then
      local order = 
      {
        OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
        UnitIndex = GameRules.earthSpirit:entindex(),
        Position = tossTarget:GetOrigin(),
        TargetIndex = tossTarget:entindex(),
        AbilityIndex = GameRules.earthSpirit:GetAbilityByIndex(2):entindex()
      }
      ExecuteOrderFromTable( order )
    end

    bossAi.curCastTime = 1.6
    return 1
  end

  GameRules.earthSpirit:SetAbsOrigin(Vector(0,0 -10000))
  bossAi.curPattern = nil
  return
end

function PudgeBoss:Crush()
  local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, boss:GetOrigin(), nil, 300, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
  if #allEnemies > 0 then
    order =
    {
      OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
      UnitIndex = boss:entindex(),
      AbilityIndex = bossAi.abilities[5]:entindex()
    }
    ExecuteOrderFromTable( order )
  else
    bossAi.patterns[7].curCd = 2
  end

  bossAi.curPattern = nil
  return
end

function PudgeBoss:DropMiniPudge()
  local allEnemies = GameRules.heroes:GetHeroesEnt()
  if #allEnemies > 0 then
    local index = RandomInt( 1, #allEnemies )
    target = allEnemies[index]
  end

  if target then
    CreateUnitByName( "npc_dota_creature_pudge_bomb", target:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
  end

  bossAi.curPattern = nil
  return
end

function PudgeBoss:HealingTomb()
  if tombMax ~= curTombnum then
    local index = RandomInt(1, tombMax-curTombnum)
    local target
    local i = 0

    for k,v in pairs(tombsPoints) do
      i = i + 1
      if i == index then
        target = v
        tombsPoints[k] = nil

        local tomb = CreateUnitByName("npc_dota_creature_tombstone", v:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )

        curTombs[tomb:entindex()] = v
        curTombnum = curTombnum + 1
        break
      end
    end
  end

  bossAi.curPattern = nil
  return
end

function PudgeBoss:TombDeath(tomb)
  for k,v in pairs(curTombs) do
    if k == tomb then
      curTombs[k] = nil
      for i=1, tombMax, 1 do
        if not tombsPoints[i] then
          tombsPoints[i] = v
          break
        end
      end
      curTombnum = curTombnum - 1
      break
    end
  end
end

function PudgeBoss:Dismember()
  if bossAi.curCastTime == 0 then
    bossAi.curCastTime = 1
    return 1

  elseif bossAi.curCastTime == 1 then
    local target
    local minDist = 99999
    if bossAi.abilities[3] and bossAi.abilities[3]:IsFullyCastable() then
      local allEnemies = GameRules.heroes:GetHeroesEnt()
      if #allEnemies > 0 then
        for _,enemy in pairs(allEnemies) do
          local distance = ( boss:GetOrigin() - enemy:GetOrigin() ):Length()
          if distance < minDist then
            target = enemy
            minDist = distance
          end
        end
      end
    end

    if target then
      order =
      {
        OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
        UnitIndex = boss:entindex(),
        Position = target:GetOrigin(),
        TargetIndex = target:entindex(),
        AbilityIndex = bossAi.abilities[3]:entindex()
      }
      ExecuteOrderFromTable( order )
    end

    bossAi.curCastTime = 5
    return 4
  end

  bossAi.curPattern = nil
  return
end

function PudgeBoss:TrackingHook()
end