local boss
local bossAi = {}
local order
local phase
local death = true

local ships = {}
local curShip
local shipRange = 700

local curNagas = {}
local nagaPoints
local nagaMax
local curNaganum

local goodguysSpawn

if KunkkaBoss == nil then
  KunkkaBoss = class({})
  
  if not GameRules.BossScripts then GameRules.BossScripts = {} end
  GameRules.BossScripts[3] = KunkkaBoss()
  
  death = true
  Stats:setNumber("kunkka", 0)
end

function KunkkaBoss:GetBossName()
  return "KunkkaBoss"
end

function KunkkaBoss:Death ()
  death = true
  Timers:CreateTimer(10, GameRules.BossScript.DeathDestroy)
end

function KunkkaBoss:AddStats ()
  Stats:inc("kunkka", 1)
end

function KunkkaBoss:DeathDestroy()
  for _,v in pairs(ships) do v:ForceDeath() end
  
  local slarks = Entities:FindAllByModel("models/heroes/slark/slark.vmdl")
  for _,k in pairs(slarks) do if k then k:ForceKill(true) end end
  
  local nagas = Entities:FindAllByModel("models/heroes/siren/siren.vmdl")
  for _,k in pairs(nagas) do if k then k:ForceKill(true) end end
  
  local tides = Entities:FindAllByModel("models/heroes/tidehunter/tidehunter.vmdl")
  for _,k in pairs(tides) do if k then k:ForceKill(true) end end
  
  boss:Destroy()
end

function KunkkaBoss:GetDeath()
  return death
end

function KunkkaBoss:GetBoss ()
  return boss
end

function KunkkaBoss:GetBossAi ()
  return bossAi
end

function KunkkaBoss:GetQuestInitiator()
  local quest =
  {
    name  = "#DOTA_Quest_Kunkka_Init_Name",
    title = "#DOTA_Quest_Kunkka_Init_Title",
    type = 0
  }
  return quest
end

function KunkkaBoss:GetQuest()
  local quest =
  {
    name  = "#DOTA_Quest_Kunkka_Combat_Name",
    title = "#DOTA_Quest_Kunkka_Combat_Title",
    type = 2
  }
  return quest
end

function KunkkaBoss:GetQuestFinale()
  local quest =
  {
    name  = "#DOTA_Quest_Clear_Name",
    title = "#DOTA_Quest_Kunkka_Clear",
    type = 3
  }
  return quest
end

function KunkkaBoss:AddShip(entity)
  ships[#ships+1] = entity
  return #ships
end

function KunkkaBoss:GetShips()
  return ships
end

function KunkkaBoss:GetShipRange()
  return shipRange
end

function KunkkaBoss:Spawn()
  spawnPoint = Entities:FindByName(nil, "KunkkaBossSpawn")
  boss = CreateUnitByName( "npc_dota_hero_kunkka", spawnPoint:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
  
  bossAi.abilities = {}
  bossAi.abilities[1] = GameRules.earthSpirit:FindAbilityByName("kunkka_torrent")
  bossAi.abilities[4] = boss:FindAbilityByName("dota_creature_no_healbar")
  for _,v in pairs(bossAi.abilities) do
    v:SetLevel(4)
  end
  bossAi.abilities[2] = GameRules.earthSpirit:FindAbilityByName("kunkka_x_marks_the_spot")
  bossAi.abilities[2]:SetLevel(1)
  bossAi.abilities[3] = boss:FindAbilityByName("kunkka_tidebringer")
  bossAi.abilities[3]:SetLevel(1)
  
  bossAi.patternTick = 0.1
  bossAi.curPattern = nil
  bossAi.curCastTime = 0
    
  bossAi.patterns = {}
  bossAi.patterns[1] = {on=true, cd=15, curCd=15, 
                            cast=GameRules.BossScript.ChangeBoat, message="A new course!"}
  bossAi.patterns[2] = {on=true, cd=15, curCd=20, 
                            cast=GameRules.BossScript.Xmarks, message="Heh! Man overboard!"}
    bossAi.patterns[3] = {on=false, cd=35, curCd=35, 
                            cast=GameRules.BossScript.MiniSlarks}
  bossAi.patterns[4] = {on=true, cd=5, curCd=5, 
                            cast=GameRules.BossScript.Torrent}
    bossAi.patterns[5] = {on=false, cd=20, curCd=20, 
                            cast=GameRules.BossScript.NagaSiren, message="Siren, you can stop calling me now."}
  bossAi.patterns[6] = {on=true, cd=30, curCd=30, 
                            cast=GameRules.BossScript.Tidehunter, message="Look what the tide washed in."}

  ships = {}
  curNagas = {}
  
  for i=1, 3, 1 do
    CreateUnitByName( "npc_dota_creature_kunkka_ship", boss:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
  end
  curShip = 1
  
  nagaPoints = Entities:FindAllByName("naga_spawn")
  curNagas = {}
  curNaganum = 0
  nagaMax = #nagaPoints
  
  goodguysSpawn = Entities:FindByClassname(nil, "info_player_start_dota"):GetOrigin()
  
  death = false
  phase = 0
  
  Timers:CreateTimer(0.5, GameRules.BossScript.PatternManager)
  Timers:CreateTimer(0.01, GameRules.BossScript.UpdateKunkkaPostition)
  Timers:CreateTimer(2, GameRules.BossScript.GlobalRadiance)
end

function KunkkaBoss:GlobalRadiance()
  if death then return end
  local allHeroes = FindUnitsInRadius( DOTA_TEAM_BADGUYS, boss:GetOrigin(), nil, -1, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )

  for _,ship in pairs(ships) do
    for _,safeHeroes in pairs(ship:GetHeroesInRange()) do
      for k,hero in pairs(allHeroes) do
        if safeHeroes:GetEntityIndex() == hero:GetEntityIndex() then
          allHeroes[k] = nil
          break;
        end
      end
    end
  end

  damageTable = {attacker = boss, damage = 150, damage_type = DAMAGE_TYPE_PURE}

  for _,target in pairs(allHeroes) do
    damageTable.victim = target
    ApplyDamage(damageTable)
  end

  return 1
end

function KunkkaBoss:UpdateKunkkaPostition()
  if death then return end
  local update = ships[curShip]:GetSailorPosition()

  boss:SetAbsOrigin(update.pos)

  return 0.01
end

function KunkkaBoss:PatternManager ()
  if death then return end
  
  FireGameEvent( "impossible_update_boss_health", 
        { healthPercent = boss:GetHealthPercent(), curHealth = boss:GetHealth()} )
    
    if     phase == 0 and boss:GetHealthPercent() < 80 then GameRules.BossScripts[3].Phase1() 
    elseif phase == 1 and boss:GetHealthPercent() < 60 then GameRules.BossScripts[3].Phase2()
    elseif phase == 2 and boss:GetHealthPercent() < 40 then GameRules.BossScripts[3].Phase3()
    elseif phase == 3 and boss:GetHealthPercent() < 20 then GameRules.BossScripts[3].Phase4()
    end
    
  for _,k in pairs(bossAi.patterns) do
    k.curCd = k.curCd - bossAi.patternTick
  end
    
  if not bossAi.curPattern then
    local cast
    for _,k in pairs(bossAi.patterns) do
      if k.curCd <= 0 and k.on then
        cast = k
      end
    end
    
    if cast then
      if (cast.message) then
        FireGameEvent( "impossible_boss_skill", {message = cast.message} )
      end
      bossAi.curCastTime = 0
      Timers:CreateTimer(bossAi.patternTick, cast.cast)
      bossAi.curPattern = cast
      cast.curCd = cast.cd
    end
    end

  return bossAi.patternTick
end

function KunkkaBoss:Phase1()
  print("Kunkka Phase1")
  bossAi.abilities[3]:SetLevel(2)
  bossAi.patterns[3].cd = 35
  bossAi.patterns[3].on = true
  bossAi.patterns[4].cd = 0.8
  bossAi.patterns[4].on = true
    
    
  shipRange = 650
  phase = 1
end

function KunkkaBoss:Phase2()
  print("Kunkka Phase2")
  bossAi.abilities[2]:SetLevel(2)
  bossAi.abilities[3]:SetLevel(3)
  bossAi.patterns[3].cd = 25
  bossAi.patterns[4].cd = 0.6
  bossAi.patterns[5].cd = 20
  bossAi.patterns[5].on = true
  
  shipRange = 600
  phase = 2
end

function KunkkaBoss:Phase3()
  print("Kunkka Phase3")
  bossAi.abilities[2]:SetLevel(3)
  bossAi.abilities[3]:SetLevel(4)
  bossAi.patterns[3].cd = 15
  bossAi.patterns[4].cd = 0.3
  bossAi.patterns[5].cd = 20
    
  shipRange = 400
  phase = 3
end

function KunkkaBoss:Phase4()
  print("Kunkka Phase4")
  phase = 4
end

function KunkkaBoss:Torrent()
  local position
  local a = RandomFloat(-250, 250)
  local b = RandomFloat(-250, 250)
  
  local allEnemies = GameRules.heroes:GetHeroesEnt()
  if #allEnemies > 0 then
    local index = RandomInt( 1, #allEnemies )
    position = allEnemies[index]:GetOrigin()
  end
  
  if position then
    position.x = position.x + a
    position.y = position.y + b
    
    local order = 
    {
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = GameRules.earthSpirit:entindex(),
      Position = position,
      AbilityIndex = bossAi.abilities[1]:entindex()
    }
    ExecuteOrderFromTable( order )
    
    position.z = position.z + 50
    CreateUnitByName( "npc_dota_dummy_target", position, false, nil, nil, DOTA_TEAM_GOODGUYS )
  end

  bossAi.curPattern = nil
  return
end

function KunkkaBoss:MiniSlarks()
  local position = boss:GetOrigin()
  local a = RandomFloat(-300, 300)
  local b = RandomFloat(-300, 300)
  
  CreateUnitByName( "npc_dota_creature_minislark", position, true, nil, nil, DOTA_TEAM_BADGUYS )
  position.x = position.x + a
  CreateUnitByName( "npc_dota_creature_minislark", position, true, nil, nil, DOTA_TEAM_BADGUYS )
  position.y = position.y + b
  CreateUnitByName( "npc_dota_creature_minislark", position, true, nil, nil, DOTA_TEAM_BADGUYS )
  
  
  bossAi.curPattern = nil
  return
end

function KunkkaBoss:NagaSiren()
  if nagaMax ~= curNaganum then
    local index = RandomInt(1, nagaMax-curNaganum)
    local target
    local i = 0
    
    for k,v in pairs(nagaPoints) do
      i = i + 1
      if i == index then
        target = v
        nagaPoints[k] = nil
        
        local naga = CreateUnitByName("npc_dota_creature_mermaid", v:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
        
        curNagas[naga:entindex()] = v
        curNaganum = curNaganum + 1
        break
      end
    end
  end

  bossAi.curPattern = nil
  return
end

function KunkkaBoss:NagaDeath(naga)
  for k,v in pairs(curNagas) do
    if k == naga then
      curNagas[k] = nil
      for i=1, nagaMax, 1 do
        if not nagaPoints[i] then
          nagaPoints[i] = v
          break
        end
      end
      curNaganum = curNaganum - 1
      break
    end
  end
end

function KunkkaBoss:ChangeBoat()
  if bossAi.curCastTime == 0 then
    boss:AddNewModifier( boss, nil, "modifier_invisible", {} )
    bossAi.curCastTime = 1
    return 1
  elseif bossAi.curCastTime == 1 then
    curShip = RandomInt(1,#ships)
    bossAi.curCastTime = 2
    return 1
  end
  
  boss:RemoveModifierByName("modifier_invisible")
  
  bossAi.curPattern = nil
  return
end

function KunkkaBoss:Tidehunter()
  local tide = CreateUnitByName( "npc_dota_creature_tideboy", goodguysSpawn, true, nil, nil, DOTA_TEAM_GOODGUYS )
  EmitGlobalSound("tidehunter_tide_rival_04")
  tide:AddNewModifier( tide, nil, "modifier_magic_immune", {} )
  bossAi.curPattern = nil
  return
end

function KunkkaBoss:Xmarks()
  if bossAi.abilities[2] and bossAi.abilities[2]:IsFullyCastable() then
    local allEnemies = GameRules.heroes:GetHeroesEnt()
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end
  
  if target and not target:IsNull() then
    order =
    {
      OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
      UnitIndex = GameRules.earthSpirit:entindex(),
      Position = target:GetOrigin(),
      TargetIndex = target:entindex(),
      AbilityIndex = bossAi.abilities[2]:entindex()
    }
    ExecuteOrderFromTable( order )
  else
    bossAi.patterns[2].curCd = 1
  end
  
  bossAi.curPattern = nil
  return
end