local boss
local bossAi = {}
local order
local phase
local death = true
 
if RubickBoss == nil then
  RubickBoss = class({})
  
  if not GameRules.BossScripts then GameRules.BossScripts = {} end
  GameRules.BossScripts[1] = RubickBoss()
  
  death = true
  Stats:setNumber("rubick", 0)
end

function RubickBoss:GetBossName()
  return "RubickBoss"
end

function RubickBoss:Death ()
  death = true
  Timers:CreateTimer(10, GameRules.BossScript.DeathDestroy)
end

function RubickBoss:AddStats ()
  Stats:inc("rubick", 1)
end

function RubickBoss:DeathDestroy()
  local wards = Entities:FindAllByModel("models/heroes/shadowshaman/shadowshaman_totem.vmdl")
  for _,k in pairs(wards) do if k then k:ForceKill(true) end end
  boss:Destroy()
end

function RubickBoss:GetDeath()
  return death
end

function RubickBoss:GetBoss ()
  return boss
end

function RubickBoss:GetBossAi ()
  return bossAi
end

function RubickBoss:GetQuestInitiator()
  local quest =
  {
    name  =  "#DOTA_Quest_Rubick_Init_Name",
    title =  "#DOTA_Quest_Rubick_Init_Title",
    type = 0
  }
  return quest
end

function RubickBoss:GetQuest()
  local quest =
  {
    name  = "#DOTA_Quest_Rubick_Combat_Name",
    title = "#DOTA_Quest_Rubick_Combat_Title",
    type = 2
  }
  return quest
end

function RubickBoss:GetQuestFinale()
  local quest =
  {
    name  = "#DOTA_Quest_Clear_Name",
    title = "#DOTA_Quest_Rubick_Clear",
    type = 3
  }
  return quest
end

function RubickBoss:Spawn()
  spawnPoint = Entities:FindByName(nil, "RubickBossSpawn")
  boss = CreateUnitByName( "npc_dota_hero_rubick", spawnPoint:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
  
  bossAi.abilities = {}
  bossAi.abilities[1] = boss:FindAbilityByName("rubick_boss_earth_toten")
  bossAi.abilities[2] = boss:FindAbilityByName("rubick_boss_sphere")
  bossAi.abilities[3] = boss:FindAbilityByName("lina_light_strike_array")
  bossAi.abilities[4] = boss:FindAbilityByName("batrider_firefly")
  bossAi.abilities[5] = boss:FindAbilityByName("sandking_burrowstrike")
  bossAi.abilities[6] = boss:FindAbilityByName("rubick_boss_fire_ward")
  bossAi.abilities[7] = boss:FindAbilityByName("dota_creature_no_healbar")
  for _,v in pairs(bossAi.abilities) do
  v:SetLevel(4)
  end
  
  bossAi.patternTick = 0.1
  bossAi.curPattern = nil
  bossAi.curCastTime = 0
  
  bossAi.patterns = {}
  bossAi.patterns[1] = {on=true, cd=0.25, curCd=0.25, 
                            cast=GameRules.BossScript.Missile}
  bossAi.patterns[2] = {on=true, cd=10, curCd=10, 
                            cast=GameRules.BossScript.LinaStun, message="FEEL THE FIRE!" }
  bossAi.patterns[3] = {on=true, cd=15, curCd=15, 
                            cast=GameRules.BossScript.RadialBurst, message="Spinning Missiles!"}
  bossAi.patterns[4] = {on=true, cd=5, curCd=5,
                            cast=GameRules.BossScript.EarthToten}
  bossAi.patterns[5] = {on=true, cd=40, curCd=40, 
                            cast=GameRules.BossScript.Burrowstrike, message="I merge with the sand!"}
  bossAi.patterns[6] = {on=false, cd=100, curCd=0, 
                            cast=GameRules.BossScript.FireFly}
  bossAi.patterns[7] = {on=false, cd=9, curCd=9, 
                            cast=GameRules.BossScript.FireWard, message="Serrrpents arise!"}
  
  death = false
  phase = 0
  
  Timers:CreateTimer(0.5, GameRules.BossScript.PatternManager)
end

function RubickBoss:Phase1()
  bossAi.patterns[1].cd = 0.20
  bossAi.patterns[6].on = true
  
  phase = 1
end

function RubickBoss:Phase2()
  bossAi.patterns[7].on = true
  
  phase = 2
end

function RubickBoss:Phase3()
  bossAi.patterns[1].cd = 0.15
  bossAi.patterns[7].cd = 2
  
  phase = 3
end

function RubickBoss:Phase4()
  phase = 4
end

function RubickBoss:PatternManager ()
  if death then return end
    
  if     phase == 0 and boss:GetHealthPercent() < 80 then GameRules.BossScripts[1].Phase1() 
  elseif phase == 1 and boss:GetHealthPercent() < 60 then GameRules.BossScripts[1].Phase2()
  elseif phase == 2 and boss:GetHealthPercent() < 40 then GameRules.BossScripts[1].Phase3()
  elseif phase == 3 and boss:GetHealthPercent() < 20 then GameRules.BossScripts[1].Phase4()
  end

  FireGameEvent( "impossible_update_boss_health", { healthPercent = boss:GetHealthPercent(), curHealth = boss:GetHealth()} )
  
  for _,k in pairs(bossAi.patterns) do
  k.curCd = k.curCd - bossAi.patternTick
  end
  
  if not bossAi.curPattern then
    local cast
    for _,k in pairs(bossAi.patterns) do
        if k.curCd <= 0 and k.on then
    cast = k
    end
  end
  
  if cast then
    if (cast.message) then
      FireGameEvent( "impossible_boss_skill", {message = cast.message} )
    end
    bossAi.curCastTime = 0
    Timers:CreateTimer(bossAi.patternTick, cast.cast)
    bossAi.curPattern = cast
    cast.curCd = cast.cd
    end
  end
  
  return bossAi.patternTick
end

function RubickBoss:FireFly ()
  if death then return end
  if bossAi.abilities[4] and bossAi.abilities[4]:IsFullyCastable() then
  order =
  {
    queue = true,
    OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
    UnitIndex = boss:entindex(),
    AbilityIndex = bossAi.abilities[4]:entindex()
  }
  ExecuteOrderFromTable( order )
  end
  
  bossAi.curPattern = nil
  return
end

function RubickBoss:Burrowstrike ()
  if death then return end
  local target = boss:GetOrigin()
  local a = RandomInt(0, 1200)
  local b = RandomInt(0, 1200)
  
  target.x = target.x + a - 600
  target.y = target.y + b - 600
  
  order =
  {
    queue = true,
    OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
    UnitIndex = boss:entindex(),
    Position = target,
    AbilityIndex = bossAi.abilities[5]:entindex()
  }
  ExecuteOrderFromTable( order )
  
  bossAi.curCastTime = bossAi.curCastTime + 1
  
  if bossAi.curCastTime >= 3 then
    bossAi.curPattern = nil
    return
  end
  
  return 1
end

function RubickBoss:EarthToten ()
  if death then return end
  local target
  local a = RandomFloat(0, 500)
  
  if bossAi.abilities[1] and bossAi.abilities[1]:IsFullyCastable() then
    local allEnemies = GameRules.heroes:GetHeroesEnt()
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end
  
  if target then
    local position = target:GetOrigin()
    position.x = position.x + a - 250
    position.y = position.y + a - 250
    order =
    {
      queue = true,
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = boss:entindex(),
      Position = position,
      AbilityIndex = bossAi.abilities[1]:entindex()
    }
    ExecuteOrderFromTable( order )
  end
  
  bossAi.curPattern = nil
  return
end

function RubickBoss:FireWard ()
  if death then return end
  local target
  local a = RandomFloat(0, 500)

  if bossAi.abilities[6] and bossAi.abilities[6]:IsFullyCastable() then
    local allEnemies = GameRules.heroes:GetHeroesEnt()
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end

  if target then
    local position = target:GetOrigin()
    position.x = position.x + a - 250
    position.y = position.y + a - 250
    order =
    {
      queue = true,
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = boss:entindex(),
      Position = position,
      AbilityIndex = bossAi.abilities[6]:entindex()
    }
    ExecuteOrderFromTable( order )
  end

  bossAi.curPattern = nil
  return
end

function RubickBoss:Missile()
  if death then return end
  local target

  if bossAi.abilities[2] and bossAi.abilities[2]:IsFullyCastable() then
    local allEnemies = GameRules.heroes:GetHeroesEnt()
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end

  if target then
    local position = target:GetOrigin() - boss:GetOrigin()
    local len = position:Length()
    position = target:GetOrigin() + (position / len * 400)

    order =
    {
      queue = true,
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = boss:entindex(),
      Position = target:GetOrigin(),
      TargetIndex = target:entindex(),
      AbilityIndex = bossAi.abilities[2]:entindex()
    }
    ExecuteOrderFromTable( order )
  end
  
  bossAi.curPattern = nil
  return
end

function RubickBoss:LinaStun()
  if death then return end
  local target
  
  if bossAi.abilities[3] and bossAi.abilities[3]:IsFullyCastable() then
    local allEnemies = GameRules.heroes:GetHeroesEnt()
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end
  
  if target then
    order =
    {
      queue = true,
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = boss:entindex(),
      Position = target:GetOrigin(),
      TargetIndex = target:entindex(),
      AbilityIndex = bossAi.abilities[3]:entindex()
    }
    ExecuteOrderFromTable( order )
  end
  
  bossAi.curPattern = nil
  return
end

function RubickBoss:RadialBurst()
  if death then return end
  local target = Vector( 1, 0, 0)
  local rot = QAngle (0, 333 * bossAi.curCastTime, 0)
  
  target =  RotatePosition(Vector(0,0,0), rot, target)
  target = boss:GetOrigin() + target * 900
  
  if target then
    order =
    {
      queue = true,
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = boss:entindex(),
      Position = target,
      AbilityIndex = bossAi.abilities[2]:entindex()
    }
    ExecuteOrderFromTable( order )
  end
  
  bossAi.curCastTime = bossAi.curCastTime + bossAi.patternTick
  
  if bossAi.curCastTime > 2.5 then
  bossAi.curPattern = nil
  return
  end
  
  return bossAi.patternTick
end