local hook
 
function Precache( context )
end

-- Create the game mode when we activate
function Activate()
  hook = thisEntity:FindAbilityByName("pudge_meat_hook")
  Timers:CreateTimer(0.2, Hold)
  Timers:CreateTimer(2.5, HookDem)
  
  order =
  {
    OrderType = DOTA_UNIT_ORDER_CAST_TOGGLE,
    UnitIndex = thisEntity:entindex(),
    AbilityIndex = thisEntity:FindAbilityByName("pudge_rot"):entindex()
  }
  ExecuteOrderFromTable( order )
end

function Hold()
  order =
  {
    OrderType = DOTA_UNIT_ORDER_HOLD_POSITION,
    UnitIndex = thisEntity:entindex(),
  }
  ExecuteOrderFromTable( order )
end

-- Evaluate the state of the game
function HookDem()
  if GameRules.BossScript:GetDeath() or not thisEntity then return end
  
  local target
  
  if hook and hook:IsFullyCastable() then
    local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end
  
  if target then
    order =
    {
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = thisEntity:entindex(),
      Position = target:GetOrigin(),
      TargetIndex = target:entindex(),
      AbilityIndex = hook:entindex()
    }
    ExecuteOrderFromTable( order )
    return RandomInt(3,8)
  else
    return 0.25
  end
end

function SpawnPudgerinos( context )
  local target = Vector( 1, 0, 0)
  local rot = QAngle (0, 360 / context.Qnt, 0)
  for i=1, context.Qnt, 1 do
    target =  RotatePosition(Vector(0,0,0), rot, target)
    
    local position = GameRules.BossScript:GetBoss():GetOrigin() + target * 900
    
    CreateUnitByName( "npc_dota_creature_minipudge", position, true, nil, nil, DOTA_TEAM_BADGUYS )
  end
end