local fireBreath
 
function Precache( context )
end

function Activate()
  fireBreath = thisEntity:FindAbilityByName("dragon_knight_breathe_fire")
  Timers:CreateTimer(0.25, FireWard)
end

function FireWard()
  if GameRules.BossScript:GetDeath() or thisEntity:IsNull() then return end
  
  local target
  
  if fireBreath and fireBreath:IsFullyCastable() then
    local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil, 
                          600, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
  end
  
  
  if target then
    order =
    {
      OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
      UnitIndex = thisEntity:entindex(),
      Position = target:GetOrigin(),
      TargetIndex = target:entindex(),
      AbilityIndex = fireBreath:entindex()
    }
    ExecuteOrderFromTable( order )
    return 1
  else
    return 0.25
  end
end