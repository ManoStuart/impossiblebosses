function Precache( context )
end

-- Create the game mode when we activate
function Activate()
  ParticleManager:CreateParticle("particles/units/heroes/hero_bounty_hunter/bounty_hunter_track_trail.vpcf",
                      PATTACH_ABSORIGIN, thisEntity)

  Timers:CreateTimer(1.6, Death)
  Timers:CreateTimer(0.01, Move)
end

function Move ()
  local position = thisEntity:GetOrigin()
  position.z = -10000
  thisEntity:SetAbsOrigin(position)
end

function Death()
  thisEntity:ForceKill(true)
end