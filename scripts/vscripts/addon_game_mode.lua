require( "boss_manager" )
require( "round_tracker" )
require( "timers" )
require( "stats" )


if CAddonTemplateGameMode == nil then
  CAddonTemplateGameMode = class({})
end

function Precache( context )
  -- Rubick Boss precache
  PrecacheUnitByNameSync("npc_dota_hero_rubick",context)
  PrecacheUnitByNameSync("npc_dota_hero_earth_spirit",context)
  PrecacheUnitByNameSync("npc_dota_hero_batrider",context)
  PrecacheUnitByNameSync("npc_dota_hero_lina",context)
  PrecacheUnitByNameSync("npc_dota_hero_sand_king",context)
  PrecacheUnitByNameSync("npc_dota_hero_puck",context)
  PrecacheUnitByNameSync("npc_dota_hero_shadow_shaman",context)
  PrecacheUnitByNameSync("npc_dota_hero_dragon_knight",context)
  
  -- Pudge Boss precache
  PrecacheResource( "particle_folder", "particles/units/heroes/hero_bounty_hunter", context )
  PrecacheUnitByNameSync("npc_dota_hero_tiny",context)
  PrecacheUnitByNameSync("npc_dota_hero_pudge",context)
  PrecacheUnitByNameSync("npc_dota_hero_bloodseeker",context)
  PrecacheUnitByNameSync("npc_dota_hero_slardar",context)
  
  -- Kunkka Boss precache
  PrecacheUnitByNameSync("npc_dota_hero_kunkka",context)
  PrecacheUnitByNameSync("npc_dota_hero_slark",context)
  PrecacheUnitByNameSync("npc_dota_hero_naga_siren",context)
  PrecacheUnitByNameSync("npc_dota_hero_tidehunter",context)
  PrecacheUnitByNameSync("npc_dota_hero_gyrocopter",context)
  PrecacheUnitByNameSync("npc_dota_hero_sniper",context)
  PrecacheResource( "model", "models/items/sniper/machine_gun_charlie/machine_gun_charlie_bullet.vmdl", context )
  PrecacheResource( "model", "models/heroes/gyro/gyro_guns.vmdl", context )
  PrecacheResource( "model", "models/heroes/kunkka/ghostship.vmdl", context )
  PrecacheResource( "soundfile", "soundevents/voscripts/game_sounds_vo_tidehunter.vsndevts", context )
end

-- Create the game mode when we activate
function Activate()
  GameRules.ImpossibleBossesMod = CAddonTemplateGameMode()
    
  GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_GOODGUYS, 10)
  GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_BADGUYS, 0)
  GameRules:SetSameHeroSelectionEnabled(true)
  GameRules:SetPreGameTime(0)
  GameRules:SetHeroSelectionTime(0)
  GameRules:SetHeroRespawnEnabled(false)
  GameRules:SetFirstBloodActive(false)
  GameRules:SetGoldPerTick(0)
  GameRules:SetUseUniversalShopMode(true)
  GameRules:SetGoldTickTime( 60.0 )
  GameRules:SetGoldPerTick( 0 )
  GameRules:SetUseCustomHeroXPValues(true)
  GameRules:SetUseBaseGoldBountyOnHeroes(true)
  GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_GOODGUYS, 10)
  GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_BADGUYS, 0)
  GameRules:LockCustomGameSetupTeamAssignment(true)
  GameRules:SetCustomGameSetupAutoLaunchDelay(3)
  
  GameRules:GetGameModeEntity():SetBuybackEnabled(false)
  GameRules:GetGameModeEntity():SetRecommendedItemsDisabled(true)
  GameRules:GetGameModeEntity():SetFogOfWarDisabled(true)
  GameRules:GetGameModeEntity():SetRemoveIllusionsOnDeath( true )
  GameRules:GetGameModeEntity():SetTopBarTeamValuesOverride( true )
  GameRules:GetGameModeEntity():SetTopBarTeamValuesVisible( false )
  
  ListenToGameEvent("dota_player_killed", HeroKilled, nil)
  ListenToGameEvent("player_spawn", PlayerSpawn, nil)
  ListenToGameEvent("entity_hurt", OnHurtPlayer, nil)
  
  Stats:auth("c4JHdey4G3fj2fEtb", "qpDjHBu65kcmoPoaH")
  Stats:setTag("map", "impossible_map")
  Stats:setNumberTag("version", 1)
  Stats:setNumberTag("debug", 1)
end