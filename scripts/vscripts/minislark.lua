local pounce

function Precache( context )
end

-- Create the game mode when we activate
function Activate()
  pounce = thisEntity:FindAbilityByName("slark_pounce")
  
  Timers:CreateTimer(1.5, CheckPounce)
  Timers:CreateTimer(0.01, Attack)
end

function CheckPounce ()
  if GameRules.BossScript:GetDeath() or thisEntity:IsNull() then return end
  
  local target
  local enemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil,
                    500, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
  if #enemies > 0 then
    local index = RandomInt( 1, #enemies )
    target = enemies[index]
  end
  
  
  if target then
    local order =
    {
      OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
      UnitIndex = thisEntity:entindex(),
      AbilityIndex = pounce:entindex()
    }
    ExecuteOrderFromTable( order )
    
    return 10
  else
    return 0.3
  end
end

function Attack()
  local target
  local enemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil,
                    -1, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
  if #enemies > 0 then
    local index = RandomInt( 1, #enemies )
    target = enemies[index]
  end
    
  if target then
    local order =
    {
      OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
      UnitIndex = thisEntity:entindex(),
      Position = target:GetOrigin(),
      TargetIndex = target:entindex()
    }
    ExecuteOrderFromTable( order )
  end
end