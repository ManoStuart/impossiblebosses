function Precache( context )
end

function Activate()
  Timers:CreateTimer(1.5, SpawnZombies)
end

function SpawnZombies()
  if GameRules.BossScript:GetDeath() or not thisEntity then return end
  
  order =
  {
    OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
    UnitIndex = thisEntity:entindex(),
    AbilityIndex = thisEntity:FindAbilityByName("tombstone_spawn_zombies"):entindex()
  }
  ExecuteOrderFromTable( order )
  
  return 10
end

function DeathReport(context)
  GameRules.BossScript:TombDeath(context.caster:entindex())
end