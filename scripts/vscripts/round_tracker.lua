local lastHealth
local playersDamages = {}

if tracker == nil then
  tracker = class({})
  GameRules.tracker = tracker()
end

function tracker:StartUp (hard)
  lastHealth = (GameRules.BossScript.GetBoss()):GetHealth()
  playersDamages = {}
end

function OnHurtPlayer( trigger )
  if GameRules.BossScript:GetDeath() then return end
  local boss = (GameRules.BossScript.GetBoss())

  if boss and boss:GetEntityIndex() == trigger.entindex_killed then
    if not playersDamages[trigger.entindex_attacker .. ""] then
      playersDamages[trigger.entindex_attacker .. ""] = 0
    end
    playersDamages[trigger.entindex_attacker .. ""] = playersDamages[trigger.entindex_attacker .. ""] + lastHealth - (GameRules.BossScript.GetBoss()):GetHealth()
    lastHealth = (GameRules.BossScript.GetBoss()):GetHealth()

  end
end

function tracker:PrintDpser()
  local dpser = nil
  local message = "Damage Ranking:<br>"
  local loopbreaker = false
  
  while true do
    for k,v in pairs(playersDamages) do
      if not dpser or playersDamages[dpser] < v then
        dpser = k
        loopbreaker = true
      end
    end
    
    if not loopbreaker then break end
    
    for k,v in pairs(GameRules.heroes.GetHeroes()) do
      if dpser.."" == k.."" then
        -- PRINT MESSAGE
        message = message.."       "..PlayerResource:GetPlayerName(v).."("..TranslateHeroName(v)..
          ")   with   " .. playersDamages[dpser..""] .. "<br>"
        
        -- RECORD HEROS DPS
        Stats:inc(PlayerResource:GetSelectedHeroName(v):sub(15) .. "Dmg", playersDamages[dpser..""])
        break
      end
    end
  
  loopbreaker = false
  playersDamages[dpser..""] = nil
  dpser = nil
  end
  
  GameRules:SendCustomMessage(message, DOTA_UNIT_TARGET_TEAM_BOTH, 15)
end

function TranslateHeroName(id)
  local str = PlayerResource:GetSelectedHeroName(id)
  if string.find(str, "zuus") then
    return "Zeus"
  elseif string.find(str, "windrunner") then
    return "Windrunner"
  elseif string.find(str, "undying") then
    return "Undying"
  elseif string.find(str, "omniknight") then
    return "Omniknight"
  elseif string.find(str, "alchemist") then
    return "Alchemist"
  end

  return str
end