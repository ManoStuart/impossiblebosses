function Precache( context )
end

-- Create the game mode when we activate
function Activate()
  Timers:CreateTimer(0.01, Charge)
end

function Charge ()
  local order =
  {
    OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
    UnitIndex = thisEntity:entindex(),
    Position = GameRules.BossScript:GetBoss():GetOrigin(),
    TargetIndex = GameRules.BossScript:GetBoss():entindex(),
  }
  ExecuteOrderFromTable( order )
end