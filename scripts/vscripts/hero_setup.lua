local heroesDone = {}
local heroesEnt = {}
local spawned = 0

if heroes == nil then
  heroes = class({})
end

function Activate()
  GameRules.heroes = heroes()
end

function Precache( context )
  -- ZUUS
  PrecacheUnitByNameSync("npc_dota_hero_skywrath_mage", context)
  PrecacheUnitByNameSync("npc_dota_hero_witch_doctor", context)
  PrecacheUnitByNameSync("npc_dota_hero_visage", context)
  
  -- ALCHMIST
  PrecacheUnitByNameSync("npc_dota_hero_phantom_lancer", context)
  PrecacheUnitByNameSync("npc_dota_hero_troll_warlord", context)
  PrecacheUnitByNameSync("npc_dota_hero_spectre", context)
  PrecacheUnitByNameSync("npc_dota_hero_riki", context)
  
  -- UNDYING
  PrecacheUnitByNameSync("npc_dota_hero_ogre_magi", context)
  PrecacheUnitByNameSync("npc_dota_hero_legion_commander", context)
  PrecacheUnitByNameSync("npc_dota_hero_bane", context)
  PrecacheUnitByNameSync("npc_dota_hero_spirit_breaker", context)
  PrecacheUnitByNameSync("npc_dota_hero_undying", context)
  
  -- WINDRUNNER
  PrecacheUnitByNameSync("npc_dota_hero_ursa", context)
  PrecacheUnitByNameSync("npc_dota_hero_templar_assassin", context)
  PrecacheUnitByNameSync("npc_dota_hero_enchantress", context)
  PrecacheUnitByNameSync("npc_dota_hero_skeleton_king", context)
  PrecacheUnitByNameSync("npc_dota_hero_faceless_void", context)
  PrecacheUnitByNameSync("npc_dota_hero_windrunner", context)
  
  -- OMNIKNIGHT
  PrecacheUnitByNameSync("npc_dota_hero_dazzle", context)
  PrecacheUnitByNameSync("npc_dota_hero_pugna", context)
end

function HeroSetup(trigger)
  local id = trigger.activator:GetPlayerID()
  
  if id ~= -1 and not heroesDone[trigger.activator:GetEntityIndex()] then
    -- SET ABILITIES
    trigger.activator:UpgradeAbility(trigger.activator:GetAbilityByIndex(0))
    trigger.activator:GetAbilityByIndex(0):SetLevel(4)
    trigger.activator:GetAbilityByIndex(1):SetLevel(4)
    trigger.activator:GetAbilityByIndex(2):SetLevel(4)
    trigger.activator:GetAbilityByIndex(3):SetLevel(4)
    trigger.activator:GetAbilityByIndex(4):SetLevel(3)
    
    -- SET GOLD
    PlayerResource:SetGold(id, 5500, false)
    
    heroesDone[trigger.activator:GetEntityIndex()] = id
    heroesEnt[#heroesEnt + 1] = trigger.activator
    spawned = spawned + 1
    
    -- RECORD HERO SELECTIONS
    Stats:inc(PlayerResource:GetSelectedHeroName(id):sub(15), 1)
    
    -- SPAWN BOSS
    if spawned == GameRules.ImpossibleBossesMod.numPlayers then
      GameRules.BossScript.SetQuest()
    end
  end
end

function heroes:GetHeroes()
  return heroesDone
end

function heroes:Reset()
  heroesDone = {}
  heroesEnt = {}
  spawned = 0
end

function heroes:GetHeroesEnt()
  return heroesEnt
end