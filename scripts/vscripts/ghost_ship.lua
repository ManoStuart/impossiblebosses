local guns
local cannon
local cannonAngle

local numShip
local waypoints
local curWayp

local barrage
local assassinate
local assCD


if GhostShip == nil then
  GhostShip = class({})
end

function Precache( context )
end

-- Create the game mode when we activate
function Activate()
  guns = CreateUnitByName( "npc_dota_creature_kunkka_ship_guns", 
              thisEntity:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
  cannon = CreateUnitByName( "npc_dota_creature_kunkka_ship_cannon", 
              thisEntity:GetOrigin(), true, nil, nil, DOTA_TEAM_BADGUYS )
  
  cannonAngle = QAngle(0,-90,0)
  assCD = 0
  
  barrage = guns:FindAbilityByName("gyrocopter_rocket_barrage")
  barrage:SetLevel(4)
  guns:FindAbilityByName("gyrocopter_rocket_barrage"):SetLevel(1)
  
  assassinate = cannon:FindAbilityByName("dota_creature_no_minimap")
  assassinate:SetLevel(4)
  cannon:FindAbilityByName("dota_creature_no_minimap"):SetLevel(1)
  
  numShip = GameRules.BossScript:AddShip(GhostShip())
  waypoints = {}
  i = 0
  while true do
    local wayp = Entities:FindByName(nil, "gs_"..tostring(numShip).."_"..tostring(i))
    if not wayp then break end
    
    i = i + 1
    waypoints[i] = wayp
  end
  curWayp = 1
  
  thisEntity:SetModelScale(2)
  cannon:SetModelScale(4)
  local ships = GameRules.BossScript:GetShips()
  
  Timers:CreateTimer(0.1, ships[numShip].UpdateAtachPositions)
  Timers:CreateTimer(6.5, ships[numShip].RocketBarrage)
  Timers:CreateTimer(6.5, ships[numShip].Assassinate)
  Timers:CreateTimer(0.01, ships[numShip].Move)
end

function GhostShip:RocketBarrage()
  if GameRules.BossScript:GetDeath() or not guns then return end
  
  local order =
  {
    OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
    UnitIndex = guns:entindex(),
    AbilityIndex = barrage:entindex()
  }
  ExecuteOrderFromTable( order )

  return 8
end

function GhostShip:Assassinate()
  if GameRules.BossScript:GetDeath() or not cannon then return end
  
  if assCD == 0 then
    cannon:SetRenderColor(255, 180, 180)
    local target
    local position = cannon:GetOrigin() + cannon:GetForwardVector() * 250
    local allEnemies = FindUnitsInRadius( DOTA_TEAM_BADGUYS, position, nil, 
                          250, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
    if #allEnemies > 0 then
      local index = RandomInt( 1, #allEnemies )
      target = allEnemies[index]
    end
    
    if target then
      order =
      {
        OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
        UnitIndex = cannon:entindex(),
        Position = target:GetOrigin(),
        TargetIndex = target:entindex(),
        AbilityIndex = assassinate:entindex()
      }
      ExecuteOrderFromTable( order )
      
      assCD = 3
      cannon:SetRenderColor(255, 80, 80)
      return 3
    else
      return 0.1
    end
  elseif assCD == 3 then
    cannon:SetRenderColor(255, 255, 255)
    
    if RollPercentage(50) then
      cannonAngle = QAngle(0,-90,0)
    else
      cannonAngle = QAngle(0,90,0)
    end
    
    assCD = 0
    return 10
  end
end

function GhostShip:Move ()
  if GameRules.BossScript:GetDeath() or not thisEntity then return end
  
  if ((waypoints[curWayp]:GetOrigin() - thisEntity:GetOrigin()):Length() < 300) then
    curWayp = (curWayp % #waypoints) + 1
    
    local order =
    {
      OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
      UnitIndex = thisEntity:entindex(),
      Position = waypoints[curWayp]:GetOrigin(),
    }
    ExecuteOrderFromTable( order )
  end
  
  return 0.3
end

function GhostShip:UpdateAtachPositions()
  if GameRules.BossScript:GetDeath() or not thisEntity then return end
  
  local forward = thisEntity:GetForwardVector()
  
  local position = thisEntity:GetOrigin() + forward * 128
  position.z = position.z -128
  guns:SetAbsOrigin(position)
  guns:SetForwardVector(forward)
  
  forward =  RotatePosition(Vector(0,0,0), cannonAngle, forward)
  position = thisEntity:GetOrigin() + forward * 119
  position.z = position.z + 90
  cannon:SetForwardVector(forward)
  cannon:SetAbsOrigin(position)
  
  return 0.01
end

function GhostShip:GetSailorPosition()
  local forward = thisEntity:GetForwardVector()
  local position = thisEntity:GetOrigin() + forward * 65
  position.z = position.z + 162
  
  return {pos = position, forward = forward}
end

function GhostShip:GetHeroesInRange()
  return FindUnitsInRadius( DOTA_TEAM_BADGUYS, thisEntity:GetOrigin(), nil,
            GameRules.BossScript:GetShipRange(), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
end

function GhostShip:ForceDeath()
  cannon:ForceKill(true)
  guns:ForceKill(true)
  thisEntity:ForceKill(true)
end