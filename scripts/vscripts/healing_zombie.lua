function Precache( context )
end
-- Create the game mode when we activate
function Activate()
  Timers:CreateTimer(0.1, Move)
  Timers:CreateTimer(0.5, Heal)
end

function Move()
  if GameRules.BossScript:GetDeath() or not thisEntity then return end

  local order =
  {
    OrderType = DOTA_UNIT_ORDER_MOVE_TO_TARGET,
    UnitIndex = thisEntity:entindex(),
    Position = GameRules.BossScript:GetBoss():GetOrigin(),
    TargetIndex = GameRules.BossScript:GetBoss():entindex()
  }
  ExecuteOrderFromTable( order )
end

function Heal()
  if GameRules.BossScript:GetDeath() or not thisEntity then return end
  
  local length = (GameRules.BossScript:GetBoss():GetOrigin() - thisEntity:GetOrigin()):Length()
  
  if length < 150 then
    GameRules.BossScript:GetBoss():Heal(500, thisEntity)
    thisEntity:ForceKill(true)
     return
  end
  
  return 0.5
end