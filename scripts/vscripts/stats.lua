if Stats == nil then
  print ( '[Stats] creating Stats' )
  Stats = {}
  Stats.__index = Stats
end

function Stats:start()
  Stats = self
  self.stats = {}
  self.tags = {}
end

function Stats:auth(modId, key)
  self.modId = modId;
  self.key = key;
end


function Stats:setUp(stats)
  assert(type(stats) == "table", "Argument is not a table!")
  
  self.stats = stats;
end

function Stats:inc(field, num)
  if (not self.stats[field]) then self.stats[field] = 0; end
  
  assert(type(self.stats[field]) == "number", "Desired field is not a number!")
  
  self.stats[field] = self.stats[field] + num
end

function Stats:set(field, value)
  self.stats[field] = value
end

function Stats:setNumber(field, value)
  self.stats[field] = tonumber(value)
end

function Stats:setString(field, value)
  self.stats[field] = tostring(value)
end

function Stats:setTag(field, value)
  self.tags[field] = tostring(value)
end

function Stats:setNumberTag(field, value)
  self.tags[field] = tonumber(value)
end

function Stats:publish()
  local url = "http://modtracker.info/api/match/" .. self.modId .. "/" .. self.key
  
  local params = {}
  
  -- fill tags
  for k,v in pairs(self.tags) do table.insert(params, "tags." .. k .. "=" .. v) end
  -- fill stats
  for k,v in pairs(self.stats) do table.insert(params, "stats." .. k .. "=" .. v) end
  
  -- Concat params
  if (table.getn(params) > 0) then url = url .. "?" .. table.concat(params, "&") end
  
  print("[Stats] Publishing match stats")
  
  CreateHTTPRequest("POST", url):Send(
    function(result)
      if (result.StatusCode ~= 200) then
        print("[Stats] Failed to publish match: " .. result.StatusCode .. " " .. result.Body["message"])
        return
      end

      print("[Stats] Match stats successfully published")
    end
  )
end

Stats:start()