require( "stats" )

-- Boss scripts
require( "rubick_boss" )
require( "pudge_boss" )
require( "kunkka_boss" )

startingBoss = 1

if BossManager == nil then
  BossManager = class({})
  GameRules.BossManager = BossManager()
  GameRules.BossManager.playersID = {}
end

function HeroKilled( context )
  if context.PlayerID == -1 then
    GameRules.BossScript.Death()
    GameRules.BossScript.AddStats()
    GameRules.BossManager.curQuest:CompleteQuest()
    GameRules.BossManager.curQuest = SpawnEntityFromTableSynchronous("quest", 
            GameRules.BossScript.GetQuestFinale())

    GameRules.tracker:PrintDpser()
    GameRules.BossManager.curBoss = GameRules.BossManager.curBoss + 1
    GameRules.BossManager.ready = false
    Timers:CreateTimer(3, GameRules.BossManager.NextBoss)

    FireGameEvent( "impossible_disable_health_bar", nil )
    Publish()
  else
    Stats:inc("deaths", 1)
    PlayerResource:GetPlayer(context.PlayerID):SetKillCamUnit(nil)

    GameRules.BossManager.playersAlive = GameRules.BossManager.playersAlive - 1
    if GameRules.BossManager.playersAlive == 0 then
      GameRules.tracker:PrintDpser()
      GameRules.BossScript.Death()

      GameRules.BossManager.curQuest:CompleteQuest()
      GameRules.BossManager.curQuest = SpawnEntityFromTableSynchronous("quest", 
                {name = "#DOTA_Quest_Defeat_Name",   title = "#DOTA_Quest_Defeat_Title"})
      Timers:CreateTimer(0, GameRules.BossManager.ReplayGame)

      FireGameEvent( "impossible_disable_health_bar", nil )
      Publish()
    end
  end
end

function Publish()
  Stats:setNumber("time", GameRules:GetGameTime())
  Stats:publish()
end

function BossManager:NextBoss()
  if (GameRules.BossManager.curQuest) then GameRules.BossManager.curQuest:CompleteQuest() end
  
  if not GameRules.BossScripts[GameRules.BossManager.curBoss] then
    GameRules.BossManager.curQuest = SpawnEntityFromTableSynchronous("quest", 
        {name = "#DOTA_Quest_Complete_Name",   title = "#DOTA_Quest_Complete_Title"})

    for _,k in pairs(GameRules.BossManager.playersID) do
      PlayerResource:GetSelectedHeroEntity(k):ForceKill(true)
      PlayerResource:SetGold(k, 0, true)
    end

    Timers:CreateTimer(2, GameRules.BossManager.ReplayGame)
    return
  end

  for _,v in pairs(GameRules.heroes:GetHeroesEnt()) do
    if not v:IsAlive() then
      v:RespawnUnit()
      PlayerResource:SetGold(v:GetPlayerID(), 900, true)
    else
      PlayerResource:SetGold(v:GetPlayerID(), 1500, true)
    end
    v:Heal(9999, nil)
    v:GiveMana(9999)
  end

  GameRules.BossScript = GameRules.BossScripts[GameRules.BossManager.curBoss]
  Timers:CreateTimer(20, GameRules.BossManager.SetQuest)
  GameRules.BossManager.curQuest = SpawnEntityFromTableSynchronous("quest", 
        {name = "#DOTA_Quest_Shop_Name",   title = "#DOTA_Quest_Shop_Title"})
end

function BossManager:SetQuest()
  GameRules.BossManager.curQuest:CompleteQuest()
  GameRules.BossManager.curQuest = SpawnEntityFromTableSynchronous("quest", 
        GameRules.BossScript.GetQuestInitiator())
  GameRules.BossManager.ready = true
end

function BossManager:SetUp()
  GameRules.BossManager.replayTime = 30
  GameRules.BossManager.curBoss = startingBoss
  GameRules.BossScript = GameRules.BossScripts[GameRules.BossManager.curBoss]
  GameRules.BossManager.playersAlive = #GameRules.BossManager.playersID
  GameRules.BossManager.ready = true

  if (GameRules.BossManager.curQuest) then GameRules.BossManager.curQuest:CompleteQuest() end
  GameRules.BossManager.curQuest = SpawnEntityFromTableSynchronous("quest", GameRules.BossScript.GetQuestInitiator())

  GridNav:RegrowAllTrees()

  FireGameEvent( "impossible_disable_health_bar", nil )

  Stats:setUp({
    zuus = 0,
    zuusDmg = 0,
    windrunner = 0,
    windrunnerDmg = 0,
    undying = 0,
    undyingDmg = 0,
    omniknight = 0,
    omniknightDmg = 0,
    alchemist = 0,
    alchemistDmg = 0,
    rubick = 0,
    pudge = 0,
    kunkka = 0,
    players = #GameRules.BossManager.playersID,
  })
end

function BossManager:ReplayGame ()
  GameRules.BossManager.replayTime = GameRules.BossManager.replayTime - 5;
  Say(thisEntity, "The game will restart in "..GameRules.BossManager.replayTime.." seconds..", false)

  if GameRules.BossManager.replayTime == 0 then
    GameRules.BossManager:SetUp()
    GameRules:ResetToHeroSelection()
    GameRules.heroes:Reset()
    GameRules.BossManager.ready = true
    return
  end

  return 5
end

function PlayerSpawn( context )
  if context.userid  ~= -1 then
    GameRules.BossManager.playersAlive = GameRules.BossManager.playersAlive + 1
    GameRules.BossManager.playersID[GameRules.BossManager.playersAlive] = context.userid-1
    Stats:inc("players", 1)
  end
end

function BossManager:SpawnBoss(str)
  if not GameRules.BossScript then return end
  if GameRules.BossScript:GetBossName() ~= str then return end

  if (not GameRules.earthSpirit) then
    GameRules.earthSpirit = CreateUnitByName( "npc_dota_hero_earth_spirit", 
            Vector(0,-1000, 0), true, nil, nil, DOTA_TEAM_BADGUYS)
    GameRules.earthSpirit:GetAbilityByIndex(0):SetLevel(4)
    GameRules.earthSpirit:GetAbilityByIndex(1):SetLevel(4)
    GameRules.earthSpirit:GetAbilityByIndex(2):SetLevel(4)
    GameRules.earthSpirit:GetAbilityByIndex(3):SetLevel(4)
    GameRules.earthSpirit:GetAbilityByIndex(4):SetLevel(4)
  end

  if GameRules.BossScript:GetDeath() and GameRules.BossManager.ready then
    GameRules.BossManager.curQuest:CompleteQuest()
    GameRules.BossManager.curQuest = SpawnEntityFromTableSynchronous("quest", 
            GameRules.BossScript.GetQuest())
    GameRules.BossScript:Spawn()
    GameRules.tracker:StartUp()
    GameRules.BossManager.ready = false

    local boss = GameRules.BossScript:GetBoss()
    local level = (GameRules.BossManager.playersAlive * (GameRules:GetCustomGameDifficulty() + 1)) -1
    for i=1, level, 1 do boss:HeroLevelUp(false) end

    FireGameEvent( "impossible_set_health_bar", {maxHealth = GameRules.BossScript:GetBoss():GetMaxHealth()} )
  end
end


GameRules.BossManager:SetUp()