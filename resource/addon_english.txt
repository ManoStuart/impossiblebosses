"lang"
{
  "Language"    "English"
  "Tokens"
  {    
    "addon_game_name"      "Impossible Bosses"
    
    //=================================================================================================================
    // Heroes
    //=================================================================================================================
    "npc_dota_hero_windrunner"    "Windrunner"
    "npc_dota_hero_omniknight"    "Omniknight"
    "npc_dota_hero_alchemist"     "Alchemist"
    "npc_dota_hero_undying"       "Undying"
    "npc_dota_hero_zuus"          "Zeus"
    
    //=================================================================================================================
    // Alchemist
    //=================================================================================================================
    "DOTA_Tooltip_ability_riki_blink_strike"                         "Blink Charge"
    "DOTA_Tooltip_ability_riki_blink_strike_Description"             "Teleports behind the target unit, striking for bonus damage. "
    "DOTA_Tooltip_ability_riki_blink_strike_Lore"                    ""
    "DOTA_Tooltip_ability_riki_blink_strike_Note0"                   ""
    "DOTA_Tooltip_ability_riki_blink_strike_Note1"                   ""
    "DOTA_Tooltip_ability_riki_blink_strike_Note2"                   ""
    
    "DOTA_Tooltip_ability_phantom_lancer_juxtapose"                  "Juxtapose"
    "DOTA_Tooltip_ability_phantom_lancer_juxtapose_Description"      "Allows attacks to have a chance to summon illusions. "
    "DOTA_Tooltip_ability_phantom_lancer_juxtapose_Lore"             ""
    "DOTA_Tooltip_ability_phantom_lancer_juxtapose_Note0"            ""
    "DOTA_Tooltip_ability_phantom_lancer_juxtapose_Note1"            ""
    "DOTA_Tooltip_ability_phantom_lancer_juxtapose_Note2"            ""
    
    "DOTA_Tooltip_ability_spectre_dispersion"                        "Dispersion"
    "DOTA_Tooltip_ability_spectre_dispersion_Description"            "Damage inflicted is reflected on nearby enemies. "
    "DOTA_Tooltip_ability_spectre_dispersion_Lore"                   ""
    "DOTA_Tooltip_ability_spectre_dispersion_Note0"                  ""
    "DOTA_Tooltip_ability_spectre_dispersion_Note1"                  ""
    "DOTA_Tooltip_ability_spectre_dispersion_Note2"                  ""
    
    "DOTA_Tooltip_ability_troll_warlord_fervor"                      "Fervor"
    "DOTA_Tooltip_ability_troll_warlord_fervor_Description"          "Gains increased attack speed with each continuous blow on the same target. "
    "DOTA_Tooltip_ability_troll_warlord_fervor_Lore"                 ""
    "DOTA_Tooltip_ability_troll_warlord_fervor_Note0"                ""
    "DOTA_Tooltip_ability_troll_warlord_fervor_Note1"                ""
    "DOTA_Tooltip_ability_troll_warlord_fervor_Note2"                ""
    
    "DOTA_Tooltip_ability_alchemist_chemical_rage"                   "Rage"
    "DOTA_Tooltip_ability_alchemist_chemical_rage_Description"       "Reduces base attack cooldown and increases movement speed and health regeneration. "
    "DOTA_Tooltip_ability_alchemist_chemical_rage_Lore"              ""
    "DOTA_Tooltip_ability_alchemist_chemical_rage_Note0"             ""
    "DOTA_Tooltip_ability_alchemist_chemical_rage_Note1"             ""
    "DOTA_Tooltip_ability_alchemist_chemical_rage_Note2"             ""
    
    //=================================================================================================================
    // Omniknight
    //=================================================================================================================
    "DOTA_Tooltip_ability_omniknight_purification"                                  "Purification"
    "DOTA_Tooltip_ability_omniknight_purification_Description"                      "Instantly heals a friendly unit and damages all nearby enemy units. "
    "DOTA_Tooltip_ability_omniknight_purification_Lore"                             ""
    "DOTA_Tooltip_ability_omniknight_purification_Note0"                            ""
    "DOTA_Tooltip_ability_omniknight_purification_Note1"                            ""
    "DOTA_Tooltip_ability_omniknight_purification_Note2"                            ""
    
    "DOTA_Tooltip_ability_dazzle_shadow_wave"                    "Shadow Wave"
    "DOTA_Tooltip_ability_dazzle_shadow_wave_Description"        "Sends out a bolt of power that arcs between allies, healing them while damaging any units standing nearby. "
    "DOTA_Tooltip_ability_dazzle_shadow_wave_Lore"               ""
    "DOTA_Tooltip_ability_dazzle_shadow_wave_Note0"              ""
    "DOTA_Tooltip_ability_dazzle_shadow_wave_Note1"              ""
    "DOTA_Tooltip_ability_dazzle_shadow_wave_Note2"              ""
    
    "DOTA_Tooltip_ability_rubick_null_field"                    "Null Field"
    "DOTA_Tooltip_ability_rubick_null_field_Description"                    "Protects nearby allies against weaker magics, granting them magic resistance. "
    "DOTA_Tooltip_ability_rubick_null_field_Lore"                    ""
    "DOTA_Tooltip_ability_rubick_null_field_Note0"                    ""
    "DOTA_Tooltip_ability_rubick_null_field_Note1"                    ""
    "DOTA_Tooltip_ability_rubick_null_field_Note2"                    ""
    
    "DOTA_Tooltip_ability_skeleton_king_vampiric_aura"                    "Vampiric Aura"
    "DOTA_Tooltip_ability_skeleton_king_vampiric_aura_Description"        "Nearby friendly units gain hit points based on their damage dealt when attacking enemy units. "
    "DOTA_Tooltip_ability_skeleton_king_vampiric_aura_Lore"                    ""
    "DOTA_Tooltip_ability_skeleton_king_vampiric_aura_Note0"                   ""
    "DOTA_Tooltip_ability_skeleton_king_vampiric_aura_Note1"                   ""
    "DOTA_Tooltip_ability_skeleton_king_vampiric_aura_Note2"                   ""
    
    "DOTA_Tooltip_ability_pugna_life_drain"                    "Life Drain"
    "DOTA_Tooltip_ability_pugna_life_drain_Description"        "When cast on an enemy, drains health from the target enemy unit to heal yourself. When cast on an ally, will drain your own health into your ally. "
    "DOTA_Tooltip_ability_pugna_life_drain_Lore"                    ""
    "DOTA_Tooltip_ability_pugna_life_drain_Note0"                    ""
    "DOTA_Tooltip_ability_pugna_life_drain_Note1"                    ""
    "DOTA_Tooltip_ability_pugna_life_drain_Note2"                    ""
    
    //=================================================================================================================
    // Undying
    //=================================================================================================================
    "DOTA_Tooltip_ability_legion_commander_press_the_attack"                "Press the Attack!"
    "DOTA_Tooltip_ability_legion_commander_press_the_attack_Description"    "Removes debuffs and disables from the target friendly unit, and grants bonus attack speed and health regeneration for a short time. "
    "DOTA_Tooltip_ability_legion_commander_press_the_attack_Lore"           ""
    "DOTA_Tooltip_ability_legion_commander_press_the_attack_Note0"          ""
    "DOTA_Tooltip_ability_legion_commander_press_the_attack_Note1"          ""
    "DOTA_Tooltip_ability_legion_commander_press_the_attack_Note2"          ""
    
    "DOTA_Tooltip_ability_ogre_magi_bloodlust"                              "Bloodlust"
    "DOTA_Tooltip_ability_ogre_magi_bloodlust_Description"                  "Incites a frenzy in a friendly unit, increasing its movement speed and attack speed. "
    "DOTA_Tooltip_ability_ogre_magi_bloodlust_Lore"                         ""
    "DOTA_Tooltip_ability_ogre_magi_bloodlust_Note0"                        ""
    "DOTA_Tooltip_ability_ogre_magi_bloodlust_Note1"                        ""
    "DOTA_Tooltip_ability_ogre_magi_bloodlust_Note2"                        ""
    
    "DOTA_Tooltip_ability_bane_brain_sap"                         "Brain Sap"
    "DOTA_Tooltip_ability_bane_brain_sap_Description"             "Drains the vital energies of an enemy unit, dealing damage to the target while healing yourself. "
    "DOTA_Tooltip_ability_bane_brain_sap_Lore"                    ""
    "DOTA_Tooltip_ability_bane_brain_sap_Note0"                   ""
    "DOTA_Tooltip_ability_bane_brain_sap_Note1"                   ""
    "DOTA_Tooltip_ability_bane_brain_sap_Note2"                   ""
    
    "DOTA_Tooltip_ability_spirit_breaker_empowering_haste"                   "Haste"
    "DOTA_Tooltip_ability_spirit_breaker_empowering_haste_Description"       "Increases the movement speed of nearby allied units. Can be cast to improve the movement speed bonus for 6 seconds, however afterwards the passive bonus will be halved while the ability is on cooldown. "
    "DOTA_Tooltip_ability_spirit_breaker_empowering_haste_Lore"              ""
    "DOTA_Tooltip_ability_spirit_breaker_empowering_haste_Note0"             ""
    "DOTA_Tooltip_ability_spirit_breaker_empowering_haste_Note1"             ""
    "DOTA_Tooltip_ability_spirit_breaker_empowering_haste_Note2"             ""
    
    "DOTA_Tooltip_ability_undying_flesh_golem"                         "Plague Golem"
    "DOTA_Tooltip_ability_undying_flesh_golem_Description"             "Transform into a horrifying flesh golem that possesses a Plague Aura. This aura slows all enemy units within 750 range and amplifies the damage they take; the closer they are, the more damage and slow. "
    "DOTA_Tooltip_ability_undying_flesh_golem_Lore"                    ""
    "DOTA_Tooltip_ability_undying_flesh_golem_Note0"                   ""
    "DOTA_Tooltip_ability_undying_flesh_golem_Note1"                   ""
    "DOTA_Tooltip_ability_undying_flesh_golem_Note2"                   ""
    
    //=================================================================================================================
    // Windrunner
    //=================================================================================================================
    "DOTA_Tooltip_ability_templar_assassin_refraction"                    "Refraction"
    "DOTA_Tooltip_ability_templar_assassin_refraction_Description"        "Creates a dome that provides both damage and defense. The damage and avoidance effects are separate, and have a limited number of instances. "
    "DOTA_Tooltip_ability_templar_assassin_refraction_Lore"               ""
    "DOTA_Tooltip_ability_templar_assassin_refraction_Note0"              ""
    "DOTA_Tooltip_ability_templar_assassin_refraction_Note1"              ""
    "DOTA_Tooltip_ability_templar_assassin_refraction_Note2"              ""
    
    "DOTA_Tooltip_ability_ursa_overpower"                         "Overpower"
    "DOTA_Tooltip_ability_ursa_overpower_Description"             "Gain increased attack speed for a number of subsequent attacks. "
    "DOTA_Tooltip_ability_ursa_overpower_Lore"                    ""
    "DOTA_Tooltip_ability_ursa_overpower_Note0"                   ""
    "DOTA_Tooltip_ability_ursa_overpower_Note1"                   ""
    "DOTA_Tooltip_ability_ursa_overpower_Note2"                   ""
    
    "DOTA_Tooltip_ability_skeleton_king_mortal_strike"                    "Mortal Strike"
    "DOTA_Tooltip_ability_skeleton_king_mortal_strike_Description"        "Grants a chance to deal bonus damage on an attack. "
    "DOTA_Tooltip_ability_skeleton_king_mortal_strike_Lore"               ""
    "DOTA_Tooltip_ability_skeleton_king_mortal_strike_Note0"              ""
    "DOTA_Tooltip_ability_skeleton_king_mortal_strike_Note0"              ""
    
    "DOTA_Tooltip_ability_faceless_void_backtrack"                    "Backtrack"
    "DOTA_Tooltip_ability_faceless_void_backtrack_Description"        "Grants a chance to evade both physical and magical attacks. "
    "DOTA_Tooltip_ability_faceless_void_backtrack_Lore"               ""
    "DOTA_Tooltip_ability_faceless_void_backtrack_Note0"              ""
    "DOTA_Tooltip_ability_faceless_void_backtrack_Note1"              ""
    "DOTA_Tooltip_ability_faceless_void_backtrack_Note2"              ""
    
    "DOTA_Tooltip_ability_enchantress_impetus"                    "Impetus"
    "DOTA_Tooltip_ability_enchantress_impetus_Description"        "Places an enchantment on each attack while activated, causing it to deal additional damage based on how far away the target is. The farther the target, the greater the damage dealt. "
    "DOTA_Tooltip_ability_enchantress_impetus_Lore"               ""
    "DOTA_Tooltip_ability_enchantress_impetus_Note0"              ""
    "DOTA_Tooltip_ability_enchantress_impetus_Note1"              ""
    "DOTA_Tooltip_ability_enchantress_impetus_Note2"              ""
    
    //=================================================================================================================
    // Zeus
    //=================================================================================================================
    "DOTA_Tooltip_ability_zuus_arc_lightning"                                  "Arc Lightning"
    "DOTA_Tooltip_ability_zuus_arc_lightning_Description"                      "Hurls a bolt of lightning that leaps through nearby enemy units. "
    "DOTA_Tooltip_ability_zuus_arc_lightning_Lore"                             ""
    "DOTA_Tooltip_ability_zuus_arc_lightning_Note0"                            ""
    "DOTA_Tooltip_ability_zuus_arc_lightning_Note1"                            ""
    "DOTA_Tooltip_ability_zuus_arc_lightning_Note2"                            ""
    
    "DOTA_Tooltip_ability_witch_doctor_maledict"                           "Maledict"
    "DOTA_Tooltip_ability_witch_doctor_maledict_Description"               "Curses enemies in a small area, causing them to take a set amount of damage each second, as well as bursts of damage every 4 seconds based on how much health they have lost since the curse began. "
    "DOTA_Tooltip_ability_witch_doctor_maledict_Lore"                      ""
    "DOTA_Tooltip_ability_witch_doctor_maledict_Note0"                     ""
    "DOTA_Tooltip_ability_witch_doctor_maledict_Note1"                     ""
    "DOTA_Tooltip_ability_witch_doctor_maledict_Note2"                     ""
    
    "DOTA_Tooltip_ability_visage_soul_assumption"                           "Soul Assumption"
    "DOTA_Tooltip_ability_visage_soul_assumption_Description"               "Gather charges of soul essence each time nearby heroes take more than 110 damage. When the essence is released, it deals base damage as well as damage for each gathered soul charge. "
    "DOTA_Tooltip_ability_visage_soul_assumption_Lore"                      ""
    "DOTA_Tooltip_ability_visage_soul_assumption_Note0"                     ""
    "DOTA_Tooltip_ability_visage_soul_assumption_Note1"                     ""
    "DOTA_Tooltip_ability_visage_soul_assumption_Note2"                     ""
    
    "DOTA_Tooltip_ability_puck_phase_shift"                           "Phase Shift"
    "DOTA_Tooltip_ability_puck_phase_shift_Description"               "Briefly shift into another dimension to become immune from harm. "
    "DOTA_Tooltip_ability_puck_phase_shift_Lore"                      ""
    "DOTA_Tooltip_ability_puck_phase_shift_Note0"                     ""
    "DOTA_Tooltip_ability_puck_phase_shift_Note1"                     ""
    "DOTA_Tooltip_ability_puck_phase_shift_Note2"                     ""
    
    "DOTA_Tooltip_ability_skywrath_mage_mystic_flare"                      "Mystic Flare"
    "DOTA_Tooltip_ability_skywrath_mage_mystic_flare_Description"          "Deals massive damage in the area over 2.4 seconds. "
    "DOTA_Tooltip_ability_skywrath_mage_mystic_flare_Lore"                 ""
    "DOTA_Tooltip_ability_skywrath_mage_mystic_flare_Note0"                ""
    "DOTA_Tooltip_ability_skywrath_mage_mystic_flare_Note1"                ""
    "DOTA_Tooltip_ability_skywrath_mage_mystic_flare_Note2"                ""
    
    
    //=================================================================================================================
    // Quests
    //=================================================================================================================
    "DOTA_Quest_Clear_Name"                       "Boss Clear!"
    "DOTA_Quest_Defeat_Name"                      "Defeat"
    "DOTA_Quest_Defeat_Title"                     "You have been defeated!"
    "DOTA_Quest_Shop_Name"                        "Shop time!"
    "DOTA_Quest_Shop_Title"                       "Go and spend your bounty! Next quest in 20 seconds."
    "DOTA_Quest_Complete_Name"                    "Complete!"
    "DOTA_Quest_Complete_Title"                   "Well done! You have defeated everyone... for now!"
    
    
    "DOTA_Quest_Rubick_Init_Name"                 "Find the Magus!"
    "DOTA_Quest_Rubick_Init_Title"                "Find the King's Throne and summon the Magus!"
    "DOTA_Quest_Rubick_Combat_Name"               "Kill the Magus!"
    "DOTA_Quest_Rubick_Combat_Title"              "Defeat the Grand Magus!"
    "DOTA_Quest_Rubick_Clear"               "Congratulations on killing Rubick!"
    
    "DOTA_Quest_Pudge_Init_Name"                 "Find the Butcher!"
    "DOTA_Quest_Pudge_Init_Title"                "Find the Graveyard and summon the Butcher!"
    "DOTA_Quest_Pudge_Combat_Name"               "Kill the Butcher!"
    "DOTA_Quest_Pudge_Combat_Title"              "Bury the Butcher once and for all!"
    "DOTA_Quest_Pudge_Clear"                     "Congratulations on killing Pudge!"
    
    "DOTA_Quest_Kunkka_Init_Name"                "Find the Admiral!"
    "DOTA_Quest_Kunkka_Init_Title"               "Find the sunken ship and summon the Admiral!"
    "DOTA_Quest_Kunkka_Combat_Name"              "Kill the Admiral!"
    "DOTA_Quest_Kunkka_Combat_Title"             "Sink that Admiral for good!"
    "DOTA_Quest_Kunkka_Clear"                    "Congratulations on killing Kunkka!"
    
    
    //=================================================================================================================
    // Misc
    //=================================================================================================================
    "DOTA_Item_Desc_Clash_of_the_Ancients"        ""
    "game_mode_15_desc"                           ""
  }
}
